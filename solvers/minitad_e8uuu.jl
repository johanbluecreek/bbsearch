using LinearAlgebra

⊕(a,b) = [a fill(0, (size(a,1),size(b,2))); fill(0, (size(b,1), size(a,2))) b]

const E8  = [
[2    -1   0    0    0    0    0    0];

[-1   2    -1   0    0    0    0    0];

[0    -1   2    -1   0    0    0    0];

[0    0    -1   2    -1   0    0    0];

[0    0    0    -1   2    -1   0    -1];

[0    0    0    0    -1   2    -1   0];

[0    0    0    0    0    -1   2    0];

[0    0    0    0    -1   0    0    2];
];

const U = [
[0 1];
[1 0];
];

const d = E8 ⊕ U ⊕ U ⊕ U

const w = [
    1.0,    # cplx_pen
    1.0,    # nc_pen
    25.0,   # neg_pen
    0.5,    # tad_pen
    5.0,    # diff_pen
    5.0,    # diag_pen
    50.0    # zero_pen
]

const targets = [0.0]

const fast_lll = true

const use_vecs = true

const vecs = [
    [2, 4, 6, 7, 9, 6, 3, 5, 0, 0, 1, -1, 0, 1]
]

include("./minitad_common.jl")