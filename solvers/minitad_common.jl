using BlackBoxOptim
using LinearAlgebra
using GenericLinearAlgebra
using Serialization
using Nemo
using Polynomials
import PolynomialRoots

LinearAlgebra.BLAS.set_num_threads(1)
setprecision(128 + 64)

dir = String(@__DIR__)
const e8rootsfile = dir * "/minitad_utils/e8roots.txt"

################################################################################
                  #####
                 #     #  ####  #    # #    #  ####  #    #
                 #       #    # ##  ## ##  ## #    # ##   #
                 #       #    # # ## # # ## # #    # # #  #
                 #       #    # #    # #    # #    # #  # #
                 #     # #    # #    # #    # #    # #   ##
                  #####   ####  #    # #    #  ####  #    #
################################################################################

#            ""#           #             ""#
#      mmmm    #     mmm   #mmm    mmm     #     mmm
#     #" "#    #    #" "#  #" "#      #    #    #
#     #   #    #    #   #  #   #  m"""#    #     """m
#     "#m"#    "mm  "#m#"  ##m#"  "mm"#    "mm  "mmm"
#      m  #
#       ""

# Here we define the global constants:
#
#  d - intersection matrix
#  N - dim of d
#  S - NxN matrix space
#  signs - negative signs in d
#  w - weights
#  targets - targets
#  use_vecs - selects if manually entered vectors (vecs) should be checked
#  vecs - explicit norm-squared = 2 vectors
#  early_stop - zero_pen will stop when first norm-squared = 2 is found
#  fast_lll - selects the faster round() routine of multiLLL()
#  spider - if we are performing a spider search there are many optimizations
#
# To override any of these, the suggested method is to create a new file in this
# directory, change the values of the variables you want, then add the line
# `include("./minitad_common.jl")`. Default values will then be overwritten.

⊕(a,b) = [a fill(0, (size(a,1),size(b,2))); fill(0, (size(b,1), size(a,2))) b]

if !@isdefined E8
const E8 = [
[2    -1   0    0    0    0    0    0];

[-1   2    -1   0    0    0    0    0];

[0    -1   2    -1   0    0    0    0];

[0    0    -1   2    -1   0    0    0];

[0    0    0    -1   2    -1   0    -1];

[0    0    0    0    -1   2    -1   0];

[0    0    0    0    0    -1   2    0];

[0    0    0    0    -1   0    0    2];
];
end

if !@isdefined U
const U = [
[0 1];
[1 0];
];
end

if !@isdefined d
    const d = E8 ⊕ E8 ⊕ U ⊕ U ⊕ U
end

const N = max(size(d)...)
const signs = length(findall(x -> x < 0, eigvals(d)))
const S = MatrixSpace(ZZ,N,N)

if !@isdefined w
    const w = [
        1.0,    # cplx_pen
        1.0,    # nc_pen
        25.0,   # neg_pen
        0.5,    # tad_pen
        5.0,    # diff_pen
        5.0,    # diag_pen
        50.0    # zero_pen
    ]
end

if !@isdefined targets
    const targets = [
        0.0    # tad_pen
    ]
end

if !@isdefined R
    const R, x = PolynomialRing(ZZ, "x")
end

if !@isdefined use_vecs
    use_vecs = false
end

if !@isdefined vecs
    if N == 22 && use_vecs
        if isfile(e8rootsfile)
            const vecs = open(e8rootsfile) do file
                read(file, String)
            end |> vs -> eval(Meta.parse(vs))
        else
            println("$progname: ERROR: e8roots.txt file not found.")
            exit(1)
        end
    else
        const vecs = []
    end
end

# This stops both multiLLL early, and zero_penalty() for the second matrix *if*
# one is found for the first matrix. `early_stop = true` is recommended for
# large zero_pen weights, `early_stop = false` is recommended for small zero_pen
# weights
if !@isdefined early_stop
    const early_stop = true
end

# This switch selects the faster LLL method where only round() is used. Setting
# this to false makes LLL compute all ceil/floor combinations to find the best
# reduction, which can take a lot of time. This is not recommended for large
# intersection matrices
if !@isdefined fast_lll
    const fast_lll = true
end

# If you intend to use the spider, make a file where this is set to true. The
# fitness evaluation will then stop at first encounter of a non-zero penalty,
# instead of computing all penalties that make up the fitness. This gives a
# large speed boost.
if !@isdefined spider
    const spider = false
end

# If parsed_args is not defined that means we have loaded this file in a
# jupyter notebook or something, so set up a rudimentary one
if !@isdefined parsed_args
    parsed_args = Dict()
    parsed_args["savefile"] = string(abs(rand(Int)))
    parsed_args["savepath"] = "./"
    parsed_args["distance"] = 1.0
end

#      ""#      "                         ""#
#        #    mmm    m mm           mmm     #     mmmm
#        #      #    #"  #             #    #    #" "#
#        #      #    #   #         m"""#    #    #   #
#        "mm  mm#mm  #   #    #    "mm"#    "mm  "#m"#
#                                                 m  #
#                                                  ""

"""
    inner(a,b)

Inner product of vectors `a` and `b` w.r.t. the intersection matrix `d` as the
metric. That is

    inner(a,b) = <a,b> = a^T d b
"""
function inner(a,b,d=d)::Float64
    real(conj(transpose(a)) * d * b)
end

"""
    dnorm2(a)

The norm-square of a vector `a` w.r.t. the inner product `inner()`. That is:

    dnorm2(a) = inner(a,a) = <a,a> = ||a||² = a^T d a
"""
dnorm2(a) = inner(a,a)

#                      "             m
#      mmmm    m mm  mmm    m mm   mm#mm
#      #" "#   #"  "   #    #"  #    #
#      #   #   #       #    #   #    #
#      ##m#"   #     mm#mm  #   #    "mm
#      #
#      "


"""
    mathematica_matrix(m)

Returns a string with the mathematica-formatted matrix for printing.
"""
function mathematica_matrix(m::Union{Array{Int64,2},Array{Float64,2}}, N=N)
    s = "{\n"
    for r in 1:N
        s *= "{"
        for c in 1:N
            if c != N
                s *= "$(m[r,c]), "
            else
                s *= "$(m[r,c])"
            end
        end
        if r != N
            s *= "},\n"
        else
            s *= "}\n"
        end
    end
    s *= "}\n"

    s
end

#                     m             "
#     mmmmm   mmm   mm#mm   m mm  mmm    m   m
#     # # #      #    #     #"  "   #     #m#
#     # # #  m"""#    #     #       #     m#m
#     # # #  "mm"#    "mm   #     mm#mm  m" "m

"""
    get_matrix(m[, d])

Given flux-matrix `m` and intersection matrix `d`, returns

    mdmᵀd
"""
function get_matrix(m::Union{Array{Int64,2},Array{Float64,2}},d=d)
    m * d * transpose(m) * d
end

"""
    get_second_matrix(m[, d])

Given flux-matrix `m` and intersection matrix `d`, returns

    mᵀdmd
"""
function get_second_matrix(m::Union{Array{Int64,2},Array{Float64,2}},d=d)
    transpose(m) * d * m * d
end

"""
    fixindex(j, n, i, excludes)

Given the index `j` of a coefficient matrix created by for the `i`th column and
using `excludes` returns the index for the corresponding column in `B`.
"""
function fixindex(j, n, i, excludes)
    return [k for k in 1:n if k ≠ i && k ∉ excludes][j]
end

"""
    get_offset(B,n,i,crosses,changed,excludes)

Gets the offset vector

    c_j = <a,b_j>

`a` being the `i`th column of `B`, `b_j` being the `j`th, `j ∈ 1:n` not `i` nor
included in `excludes`, using the cached value of the inner product.
"""
function get_offset(n,i,crosses,excludes)
    return [
        crosses[i,j]
    for j in 1:n if j ≠ i && j ∉ excludes]
end

"""
    get_coeffm(n,i,crosses,excludes)

Gets the coefficient matrix

    d_jk = <b_j,b_k> ;      <x,y> = inner(x,y)

`b_j` being the `j`th column of `B`, for `j,k ∈ 1:n` not `i` nor included in
`excludes` using the cached value of the inner product.
"""
function get_coeffm(n,i,crosses,excludes)
    return Symmetric(reshape([
        crosses[j,k]
    for j in 1:n, k in 1:n if (j ≠ i && k ≠ i) && (j ∉ excludes && k ∉ excludes)], (n-1-length(excludes), n-1-length(excludes))))
end

"""
    update_cross(B,j,k,crosses)

Recomputes the `crosses[j,k]`.
"""
function update_cross(B,j,k,crosses)
    crosses[j,k] = inner(B[:,j], B[:,k])
    crosses[k,j] = crosses[j,k]
end

"""
    gen_lambdas(λ)

Given a non-integer coefficient vector, it generates all unique integer
coefficient vectors with ceil/floor. Assist function for `multiLLL`.
"""
function gen_lambdas(λ)
    t = [ceil(Int64, λ[1]), floor(Int64, λ[1])] |> unique
    for i in 2:length(λ)
        t = Iterators.product(t, [ceil(Int64, λ[i]) floor(Int64, λ[i])]) |> unique |> x -> map(y -> [y[1]..., y[2]], x)
    end
    return ( typeof(t[1]) == Int64 ? map(x -> [x], t) : t ) |> x -> filter(y ->!all(iszero.(y)), x)
end

"""
    multiLLL(A)

LLL-inspired algorithm for reducing the norm of a set of vectors. This algorithm,
in contrast to `singleLLL(A)` builds the largest non-singular matrix

    d_ij = <b_i,b_j> ;      <x,y> = inner(x,y)

for each `a`, where `b_i` and `a` are columns of `A`, `b_i` not containing `a`,
and solves

    d_ij λ^j + c_i = 0 ;    c_i = <a,b_i>

producing a new vector with potentially lower norm

    a ← a + λ^i b_i

replacing `a`. The coefficients `λ^i` are checked against both ceil/floor to
find the vector closest to norm 2.

This algorithm is an improvement over `singleLLL(A)` in that is able to find many
more norm-2 vectors, missing very few that `singleLLL(A)` would have found, with
not too great inpact on performance.
"""
function multiLLL(A, early_stop=early_stop, fast_lll=fast_lll)
    B = Int64.(A)

    n = size(A,2)

    # Keep track of each vector to see if they've changed
    changed = [true for _ in 1:n]

    # Compute what is needed as needed and keep the results
    squares = Int64[ dnorm2(B[:,i]) for i in 1:n ]
    if any(squares .== 2) && early_stop
        return B
    end
    crosses = [ j == k ? squares[j]//2 : j > k ? Rational{Int64}(inner(B[:,j],B[:,k])) : 0 for j in 1:n, k in 1:n ] |> x -> Int64.(x + x')

    # Keep track of bad vectors
    excludes = [Int64[] for _ in 1:n]

    while any(changed)

        next_changed = copy(changed)

        for i in 1:n
            excludes[i] = Int64[]

            next_changed[i] = false

            # we do not need to do anything if none of the vectors has changed
            !any([changed[j] for j in 1:n if j ≠ i]) && continue

            # If we have reached 2, we stop
            if early_stop
                (squares[i] == 2) && return B
            end
            (squares[i] == 2) && continue

            # If it is a null norm, we stop
            (squares[i] == 0) && continue

            # We exclude norm = 0 for the general procedure and handle them separately.
            for j in 1:n
                if squares[j] == 0
                    push!(excludes[i], j)
                end
            end

            # the offset vector
            cv = get_offset(n,i,crosses,excludes[i])

            # Homogeneous or empty system, so we skip.
            (all(iszero.(cv)) || length(cv) == 0) && continue

            # the matrix of coefficients
            dm = get_coeffm(n,i,crosses,excludes[i])

            # singular until proven non-sigular
            singular = true
            dmi = zeros(Float64, size(dm))
            while singular
                try
                    # Try to invert the matrix
                    dmi = inv(dm)
                    singular = false
                catch e
                    if isa(e,SingularException) || isa(e,LAPACKException)
                        # The singluar exception error contains the info about the singular entry
                        push!(excludes[i], fixindex(e.info, n, i, excludes[i]))
                        # Once we have the exclude we have to redefine our `b` to make the `dm` non-singular
                        cv = get_offset(n,i,crosses,excludes[i])

                        # Homogeneous system, so we skip.
                        (all(iszero.(cv)) || length(cv) == 0) && break

                        dm = get_coeffm(n,i,crosses,excludes[i])
                        # now the while loops over, and we get continue until the matrix is non singular
                    else
                        println("Error in multiLLL algorithm: $e")
                        break
                    end
                end
            end
            # There may be a break in the above while, if so singular is true, and need to skip
            singular && continue

            # The matrix may still be singular after all that, then we need to get out
            # note that the matrix is integer, so the precision here can be set superlow
            isnzero(det(dm),1e-4) && continue

            if fast_lll

                # This determines the next step
                λ = round.(Int64, - mkzero.(dmi) * cv)
                # if it is null, there is no step
                all(λ .== 0) && continue

                new_b = B[:,i] + hcat([ B[:,j] for j in 1:n if j ≠ i && j ∉ excludes[i]]...)*λ |>
                    c -> Int64.(c .// gcd(c...))
                new_norm = dnorm2(new_b)
                if abs(new_norm - 2) < abs(squares[i] - 2)
                    # select it
                    B[:,i] = new_b
                    # update caches
                    squares[i] = new_norm
                    crosses[i,i] = squares[i]
                    for j in 1:n
                        if j ≠ i
                            update_cross(B,i,j,crosses)
                        end
                    end
                    next_changed[i] = true
                    # if we changed and reached norm = 2, we can skip the rest
                    if early_stop
                        (squares[i] == 2) && return B
                    end
                    (squares[i] == 2) && continue
                end

            else

                # Get all new potentially shorter vectors
                bs = map(
                    λ -> (B[:,i] + hcat([ B[:,j] for j in 1:n if j ≠ i && j ∉ excludes[i]]...)*λ |> gcd_normalise |> x -> Int64.(x)),
                    gen_lambdas(- mkzero.(dmi) * cv)
                )
                # if there are any
                if length(bs) > 0
                    new_norms = dnorm2.(bs)
                    # find the one with norm closest to 2
                    mini = findmin(abs.(new_norms .- 2))
                    # if this norm is closer to 2 than the original vector
                    if mini[1] < abs(squares[i] - 2)
                        # select it
                        B[:,i] = bs[mini[2]]
                        # update caches
                        squares[i] = new_norms[mini[2]]
                        crosses[i,i] = squares[i]
                        for j in 1:n
                            if j ≠ i
                                update_cross(B,i,j,crosses)
                            end
                        end
                        next_changed[i] = true
                        # if we changed and reached norm = 2, we can skip the rest
                        if early_stop
                            (squares[i] == 2) && return B
                        end
                        (squares[i] == 2) && continue
                    end
                end
            end

            # There are situations where norm = 0 vectors can lower the norm
            bs = [
                B[:,i] + B[:,j] * round(Int64, (2-squares[i])/(2*crosses[i,j]))
            for j in 1:n if j ≠ i && squares[j] == 0 && crosses[i,j] ≠ 0 ]
            if length(bs) > 0
                new_norms = dnorm2.(bs)
                # find the one with norm closest to 2
                mini = findmin(abs.(new_norms .- 2))
                # if this norm is closer to 2 than the original vector
                if mini[1] < abs(squares[i] - 2)
                    # select it
                    B[:,i] = bs[mini[2]]
                    # update caches
                    squares[i] = dnorm2(B[:,i])
                    crosses[i,i] = squares[i]
                    for j in 1:n
                        if j ≠ i
                            update_cross(B,i,j,crosses)
                        end
                    end
                    next_changed[i] = true
                    # if we changed and reached norm = 2, we can skip the rest
                    if early_stop
                        (squares[i] == 2) && return B
                    end
                    (squares[i] == 2) && continue
                end
            end
        end
        changed = next_changed
    end
    return B
end

#                m    #
#        mmm   mm#mm  # mm    mmm    m mm
#       #" "#    #    #"  #  #"  #   #"  "
#       #   #    #    #   #  #""""   #
#       "#m#"    "mm  #   #  "#mm"   #

"""
    isnzero(x[, ϵ = 1.0e-12])

Check if `x` is a numerical zero, as defined by the size `ϵ`.
"""
function isnzero(x::Number, ϵ = 1.0e-12)
    return abs(x) < ϵ
end

function isnzero(x::Array{T,1}, ϵ = 1.0e-12) where T <: Number
    return map(λ -> isnzero(λ, ϵ), x)
end

"""
    mkzero(s[, ϵ = 1.0e-12])

Makes a number zero if it is zero according to `isnzero()`.
"""
function mkzero(s::Real, ϵ = 1.0e-12)
    return isnzero(s, ϵ) ? 0 : s
end

function mkzero(s::Complex, ϵ = 1.0e-12)
    return mkzero(real(s), ϵ) + im*mkzero(imag(s), ϵ)
end

"""
    fix_matrix(x)

Fixes up a matrix from the input (`x`).
"""
function fix_matrix(x, N=N)
    return round.(Int,x) |> x -> reshape(x, (N,N))
end

"""
    do_earlystop(zero_pen)

Checks if we should do an early stop depending on `zero_pen`.
"""
function do_earlystop(zero_pen, early_stop=early_stop)
    return early_stop && zero_pen > 0
end

# verify that all the vectors are OK
for v in vecs
    safe = true
    if dnorm2(v) ≠ 2
        safe = false
        println("This vector:\n $v\nDoes not have norm 2. Remove it and re-run.")
    end
    if !safe
        exit(1)
    end
end

################################################################################
           ######
           #     # ###### #    #   ##   #      ##### # ######  ####
           #     # #      ##   #  #  #  #        #   # #      #
           ######  #####  # #  # #    # #        #   # #####   ####
           #       #      #  # # ###### #        #   # #           #
           #       #      #   ## #    # #        #   # #      #    #
           #       ###### #    # #    # ######   #   # ######  ####
################################################################################

#                "
#        mmm   mmm     mmmm         mmmm    mmm   m mm    mmm
#       #"  #    #    #" "#         #" "#  #"  #  #"  #  #
#       #""""    #    #   #         #   #  #""""  #   #   """m
#       "#mm   mm#mm  "#m"#         ##m#"  "#mm"  #   #  "mmm"
#                      m  #         #
#                       ""  """"""  "


"""
    cplx_penalty(cplx_egen, threshold=10.0, ϵ = 1.0e-12)

Given a set of complex eigenvalues it returns a penalty.
"""
function cplx_penalty(cplx_egen, threshold=10.0, ϵ = 1.0e-12)
    pen = sum(abs, imag.(cplx_egen))
    if !isnzero(pen, ϵ)
        pen += threshold
    end
    return pen
end

"""
    nc_penalty(cplx_egen, threshold=10.0, ϵ = 1.0e-12)

Given a set of complex eigenvalues it returns a penalty for the negative real
parts.
"""
function nc_penalty(cplx_egen, threshold=10.0, ϵ = 1.0e-12)
    pen = sum(abs, filter(x -> x < 0.0, real.(cplx_egen)))
    if !isnzero(pen, ϵ)
        pen += threshold
    end
    return pen
end

"""
    neg_penalty(real_egen, threshold=10.0, ϵ = 1.0e-12)

Given a set of real eigenvalues it returns a penalty for the negative ones.
"""
function neg_penalty(real_egen, threshold=10.0, ϵ = 1.0e-12)
    pen = sum(abs, filter(x -> x < 0.0, real_egen))
    if !isnzero(pen, ϵ)
        pen += threshold
    end
    return pen
end


#       m               #
#     mm#mm   mmm    mmm#         mmmm    mmm   m mm
#       #        #  #" "#         #" "#  #"  #  #"  #
#       #    m"""#  #   #         #   #  #""""  #   #
#       "mm  "mm"#  "#m##         ##m#"  "#mm"  #   #
#                                 #
#                         """"""  "


"""
    get_tadpole(mat)

Retruns tadpole corresponding to input matrix.
"""
function get_tadpole(mat)
    return abs(tr(mat)/2.0)
end

"""
    tad_penalty(tadpole, target=24.0)

Penalty corresponding to tadpole.
"""
function tad_penalty(tadpole, target=targets[1])
    return abs(tadpole - target)
end

#        #    "                       m     #    "      m""    m""
#     mmm#  mmm     mmm    mmmm      #   mmm#  mmm    mm#mm  mm#mm
#    #" "#    #        #  #" "#     #   #" "#    #      #      #
#    #   #    #    m"""#  #   #    #    #   #    #      #      #
#    "#m##  mm#mm  "mm"#  "#m"#   #     "#m##  mm#mm    #      #
#                          m  #  "
#                           ""

"""
    mkgroup(prs,egen)

Groups eigenvalues according to the multiplicities in prs, and sorts the groups.
"""
function mkgroup(prs,egen)
    pcm = [ (p,(length(p)-1)*m,m) for (p,m) in prs ]
    group = []
    calc = true
    count = 0
    for e in egen
        if calc
            #count = prs[findmin([ abs(to_poly(p)(e)) for (p,m) in prs ])[end]][end]
            index = 0
            if length(pcm) == 1
                index = 1
            else
                index = findmin([ abs(to_poly(p)(e)) for (p,c,m) in pcm ])[end]
            end
            count = pcm[index][end]

            # update pcm
            pcm[index] = (pcm[index][1], pcm[index][2]-count, pcm[index][3])
            pcm = [ (p,c,m) for (p,c,m) in pcm if c ≠ 0 ]

            push!(group,[])
        end
        push!(group[end], e)
        count -= 1
        count == 0 ? (calc = true) : (calc = false)
    end
    return group
end

"""
    get_vgroup(group, evecs)

Based on the eigenvalues grouping in `group`, group the eigenvectors in `evecs`
correspondingly.

This assumes that the eigenvectors are sorted in the same order as a flattened
`group`.
"""
function get_vgroup(group, evecs)
    ev = copy(evecs) |> e -> [ e[:,i] for i in 1:size(e,2) ]

    vgroup = [
        [
            begin a = ev[1]; deleteat!(ev,1); a end
        for i in 1:length(group[g]) ]
    for g in 1:length(group)]

    return vgroup
end

"""
    get_bilinear_forms(vgroup)

Returns the bilinear forms

    b_{ij} = <v_i,v_j>

when a set of grouped eigenvectors are more than one.
"""
function get_bilinear_forms(vgroup)
    return [[ inner(v1,v2) for v1 in vg, v2 in vg ] for vg in vgroup if length(vg) > 1]
end

"""
    diag_penalty(evecs[, δ=1e-6])

Penalty for diagonalisability given eigenvectors based on the existence of a kernel for the
eigenspace.
"""
function diag_penalty(evecs, δ=1e-6)
    if any(isnzero.(eigvals(evecs), δ))
        return 10
    end
    return 0
end

"""
    is_def_diag(prs)

If there are no non-distinct eigenvalues, then the matrix is definitely
diagonalisable.
"""
function is_def_diag(prs)
    return all([ m == 1 for (p,m) in prs])
end

"""
    diff_penalty(group, evecs, δ)

Penalty for overlapping eigenvalues, meaning there are non-distinct eigenvalues
between positive and negative norm-square eigenvectors.

We check this by grouping eigenvectors in groups of non-distinct eigenvalues,
and forming bilinears

    b_{ij} = <v_i,v_j>

whose eigenvalues must be all positive or all negative to avoid an overlap.
Hence this function returns a penalty for each zero eigenvalue of b_{ij}, and
the number of positive or negative if there is a mix of both, which ever is
smallest.
"""
function diff_penalty(group, evecs, ϵ=1.e-12)
    pen = 0
    egens = eigvals.(get_bilinear_forms(get_vgroup(group, evecs)))
    for egen in egens
        nonzeroes = [ real(e) for e in egen if !isnzero(e, ϵ) ]
        # penalty for every zero:
        pen += length(egen) - length(nonzeroes)
        pos = filter(x -> x > 0, nonzeroes)
        if length(pos) < length(nonzeroes)
            # penalty for the number of pos & neg
            pen += min(length(pos), length(nonzeroes)-length(pos))
        end
    end
    return pen
end

#      mmmmm   mmm    m mm   mmm          mmmm    mmm   m mm
#         m"  #"  #   #"  " #" "#         #" "#  #"  #  #"  #
#       m"    #""""   #     #   #         #   #  #""""  #   #
#      #mmmm  "#mm"   #     "#m#"         ##m#"  "#mm"  #   #
#                                         #
#                                 """"""  "

unitpoly(x=x) = x^0

"""
    short_charpoly(x)

Charactersitic polynomial in the polynomail ring:

    PolynomialRing(ZZ, "x")
"""
short_charpoly(mat, R=R, S=S) = charpoly(R,S(mat))

"""
    coeffs(poly)

Returns the coefficients for a Nemo.jl polynomial over the integers to be parsed
into a Polynomials.jl polynomial.
"""
function coeffs(poly::fmpz_poly)
    try
        return Int64[ coeff(poly,p) for p in 0:length(poly) ]
    catch
        return BigInt[ coeff(poly,p) for p in 0:length(poly) ]
    end
end

"""
    to_poly(poly)

Returns a Polynomials.jl polynomial given a Nemo.jl polynomial over the integers
(fmpz_poly).
"""
function to_poly(poly::fmpz_poly)
    return Polynomial(coeffs(poly), :λ)
end

"""
    null_columns(mat)

Checks if input matrix has a column all made of zeroes and returns the number of
such columns.
"""
function null_columns(mat, N=N)
    return length([ i for i in 1:N if all(iszero.(mat[:,i])) ])
end

"""
    null_vector(mat, vecs)

Checks if `vecs` are part of the null-space of `mat` and returns those that are.
"""
function null_vector(mat, vecs=vecs, early_stop=early_stop)
    if early_stop
        for e in vecs
            if all(iszero.(mat * e))
                return [e]
            end
        end
    else
        return [e for e in vecs if all(iszero.(mat * e))]
    end
    return []
end

"""
    gcd_normalise(mat)

Normalise all the columns of the matrix using the GCD of the column.
"""
function gcd_normalise(mat::Array)
    if size(mat,2) > 0
        return mapslices(c -> Int64.(c .// gcd(c...)), mat, dims = [1])
    else
        return mat
    end
end

function gcd_normalise(mat::fmpz_mat)::fmpz_mat
    if size(mat,2) > 0
        return [
                divexact(mat[:,c], gcd([ mat[e,c] for e in 1:size(mat,1) ]) |> x -> x ≠ 0 ? x : 1)
            for c in 1:size(mat,2) ] |> x -> hcat(x...)
    else
        return mat
    end
end

"""
    dnorm2s(mat)

Gives the norm-square of every column of the input matrix.
"""
function dnorm2s(mat)
    if size(mat,2) > 0
        return mapslices(c -> dnorm2(c), mat, dims = [1])
    else
        return Float64[]
    end
end

"""
    null_space(mat)

Returns the matrix for the nullspace, as computed by Nemo, as a Nemo matrix
truncted to its actual size.
"""
function null_space(mat, ZZ=ZZ, S=S)
    r, null = nullspace_right_rational(S(mat))
    S2 = MatrixSpace(ZZ,Int64(r),size(null,2))
    return null[:,1:r] |> gcd_normalise |> transpose |> S2
end

"""
    null_space_r(mat)

Returns the dimension of the nullspace.
"""
function null_space_r(mat, ZZ=ZZ, S=S)
    r, null = nullspace_right_rational(S(mat))
    return r
end

function hnf_normalised(mat, ZZ=ZZ)
   S2 = MatrixSpace(ZZ,size(mat)...)
   return transpose(hnf(mat)) |> gcd_normalise |> transpose |> S2
end

"""
    leading_elements(mat, r)

Find the leading elements of row r of the matrix mat. These are all non-zero
entries of a row such that all following elements of the corresponding column
are zero.
"""
function leading_elements(mat, r)
    return Int64[
        mat[r,i]
    for i in r:size(mat,2) if (mat[r,i] != 0 && !any(x -> x!=0, Matrix{Int}(mat[r+1:size(mat,1), i])))]
end

"""
    convertToZZ(mat)

Convert a matrix over Z/pZ to an integer matrix.
"""
function convertToZZ(mat, ZZ=ZZ)
    S2 = MatrixSpace(ZZ, size(mat,1), size(mat,2))
    m = S2()
    for i = 1:size(mat,1)
        for j = 1:size(mat,2)
            m[i,j] = ZZ(data(mat[i,j]))
        end
    end
    return m
end

"""
    prepare_modnull(null)

Bring a matrix in Z/pZ to reduced row echelon form.
"""
function prepare_modnull(null)
    Smod = parent(null)
    return rref(Smod(convertToZZ(null))) |> n -> divexact(n[3],n[2])
end

"""
    prime_reduce(mat, p)

Reduce a matrix with respect to the prime p. Divide the matrix by p and search
for a linear combination of the row vectors such that the result is integer
again. If yes, replace the respective rows by these linear combinations. This is
done by converting the elements to Z/pZ (i.e. modulus of the division with p)
and searching for a non-trivial kernel in this space.
"""
function prime_reduce(mat, p, ZZ=ZZ)
    # store the type of the input matrix and create a type for matrices in Z/pZ
    SZZ = parent(mat)
    Smod = MatrixSpace(ResidueRing(ZZ,Int(p)),size(mat,2), size(mat,1))
    # convert to Z/pZ and compute the kernel
    null = nullspace(Smod(transpose(mat)))[2]
    # bring matrix to reduced row echelon form
    null = convertToZZ(prepare_modnull(transpose(null)))
    m = Matrix{Int}(mat)
    # For each non-trivial vector r in the kernel, find its first non-vanishing
    # element and replace the respective row by the linear combination of rows
    # with coefficents r/p. The result will be integer.
    for r in eachrow(Matrix{Int}(null))
        # In principle we already determine this in prepare_modnull, so there is
        # some overhead here which can be potentially reduced.
        m[findfirst(!iszero, r),:] = Matrix{Int}(transpose(r)*m / Int(p))
    end
    return SZZ(m)
end

"""
    rational_reduce(mat)

Try to find a minimal integer basis such that all integer vectors in this space
can be expressed as integer linear combinations of this basis. Takes a matrix
whose's rows are the old integer basis vectors as input and reduces by finding
suitable rational linear combinations.
"""
function rational_reduce(mat, ZZ=ZZ)
    #m = parent(mat)(copy(Matrix{Int}(mat)))
    m = mat
    # iterate over the rows of m
    for r = 1:size(m,1)
        # Compute the prime factors of the GCD of all leading elements of this
        # row. If we divide the row by one of these prime factors all leading
        # elements stay integer. By construction, if one of the leading elements
        # would become non-integer it cannnot be made integer by adding a
        # rational linear combination of the following rows
        fac = factor(ZZ(gcd(leading_elements(m, r)...)))
        # iterate over these prime factors (including multiplicites)
        for (p, e) in fac
            for _ in 1:e
                # Reduce the block matrix consisting of this and all following
                # row with respect to the prime p
                m[r:size(m,1),:] = prime_reduce(m[r:size(m,1),:], p)
            end
        end
    end
    return m
end

"""
    red1(mat)

Reduction path 1. This chains `rational_reduce` together with `hnf` and finally
returns the norm-squares of the `multiLLL` reduction.
"""
function red1(mat)
    return mat |> rational_reduce |> hnf |> transpose |> Matrix{Int} |> multiLLL |> dnorm2s
end

"""
    red2(mat)

Reduction path 2. This has no further reduction, but returns the norm-squares of
the `multiLLL` reduction.
"""
function red2(mat)
    try
        return mat |> transpose |> Matrix{Int} |> multiLLL |> dnorm2s
    catch e
        if isa(e, ErrorException) || isa(e, InexactError)
            println("Error in red2(), skipping multiLLL; $e")
            return mat |> transpose |> Matrix{BigInt} |> dnorm2s
        else
            println("Error in red2(): $e")
            exit(1)
        end
    end
end

"""
    null_norm2s(mat)

Checks the numer of null-vectors that has norm-square equal to 2.

This is checked by:

    * If the kernel contains one vector, we check if the GCD-normalised vector
      has norm-square 2
    * If there are more vectors in the kernel we use an LLL-like algorithm.
      - First we check if the Hermite Normal Form or the kernel, GCD-normalised,
        and LLL reduced contains norm-square 2 vectors
      - If it doesn't we also check if the LLL reduction of the GCD-normalised
        kernel vectors contains norm-square 2 vectors
"""
function null_norm2s(mat)
    pen = 0
    # Every method need the null space
    null = null_space(mat)
    if size(null,1) == 1
        # If there is only one, then it is GCD normalised and we get the number
        pen += null |> Matrix{Int} |> transpose |> dnorm2s |> x -> filter(n -> n == 2, x) |> length
    elseif size(null,1) > 1
        # If there are more, we need to reduce the nullvectors a bit. First a
        # common reduction
        red = hnf_normalised(null)
        # Then our first attempt is the red1() reduction
        pen += red1(red) |> x -> filter(n -> n == 2, x) |> length
        if pen == 0
            # If red1() failed, we check with red2()
            pen += red2(red) |> x -> filter(n -> n == 2, x) |> length
        end
    end
    return pen
end

"""
    intvectors(mat)

Returns all gcd-normalised integer vectors v satisfying (`M` is the input
matrix)

    p_r(M) v = 0

where `p_r(λ)` is irreducible (over the integers) polynomial factor of the
characteristic polynomial of the input matrix

    det(M-λI) =: p(λ) = ∏_r p_r(λ)^{m_r}

and where

    (ker p_r(M)) * d * (ker p_r(M))'

is positive semi-definite.
"""
function intvectors(mat, prs, stop, ZZ=ZZ, S=S, d=d, signs=signs)
    intvecs = S()[1:-1,:]
    # if there is only one factor, we know it is bad, so do not process it
    # except for mds
    mds = []
    if length(prs) > 1 || (length(prs) == 1 && prs[1][2] > 1)
        # otherwise, keep track of all negatives found
        totnegs = 0
        for p in 1:length(prs)
            pr = prs[p][1]
            # only continue processing if not all negatives have been found
            if !(totnegs < signs && length(prs) - p == 0)
                #n = pr(S(mat)) |> null_space
                n = S(to_poly(pr)(mat)) |> null_space
                # For additional diagonal check:
                if prs[p][2] > 1
                    push!(mds, size(n,1) - (length(pr)-1)*prs[p][2])
                end
                if !stop
                    e = try
                        n |> Matrix{Int64} |> n -> eigvals(n*d*transpose(n))
                    catch
                        # Keeping this for stability, but it almost never happens.
                        # The otherway of doing it,
                        #     `eigvals(Matrix{Int64}(n*S(d)*transpose(n)))`
                        # overflows a lot though.
                        n |> Matrix{BigInt} |> n -> eigvals(n*d*transpose(n))
                    end
                    # Find the negatives
                    nonneg = filter(x -> x > -1e-9, e)
                    if length(nonneg) == length(e)
                        # If there are no negatives, vectors are good to go
                        intvecs = vcat(intvecs, n)
                    else
                        # if there are negatives, keep track of them
                        totnegs += length(e) - length(nonneg)
                    end
                end
            elseif prs[p][2] > 1
                r = S(to_poly(pr)(mat)) |> null_space_r
                push!(mds, r - (length(pr)-1)*prs[p][2])
            end
        end
    end
    return MatrixSpace(ZZ, size(intvecs)...)(intvecs), mds
end

"""
    zero_penalty()

Returns a penalty associated with egenvalues that are integers.
"""
function zero_penalty(egen, mat, prs, stop, N=N, ZZ=ZZ, use_vecs=use_vecs)
    # starting penalty is the number of columns that are zero
    pen = null_columns(mat)

    pen_contrib = 0

    mds = Int64[]
    if typeof(mat) == Array{Int64,2}

        # This reduction path was found to be the most effective during testing
        intvecs, mds = intvectors(mat,prs,stop)

        if !stop
            pen_contrib = intvecs |> rref |> x -> x[2] |> hnf_normalised |> red1 |>
                x -> filter(n -> n == 2, x) |> length
        end

    end

    # If nothing is found, we have some other tests
    if pen_contrib == 0 && !stop
        # Perform computation over every real integer eigenvalue. We also sort
        # after the multiplicity of the eigenvalue, larger first, means more
        # probable to find one, means (with early_stop) that we can stop later
        # iterations
        inteigens = round.(Int64,real.(filter(e -> isnzero(e - round(e)) && isnzero(real(e) - e), egen))) |>
            x -> begin [ (int, length(filter(y -> y == int, x))) for int in unique(x)] end |> # tally result
            x -> sort(x, lt=(a,b)->a[2]>b[2]) # sort result after largest
        for (e,m) in inteigens
            if pen_contrib == 0 && !stop

                me = mat - e*I
                pen_contrib += null_norm2s(me)

                if pen_contrib == 0 && use_vecs
                    pen_contrib += length(null_vector(me))
                end

            end

        end
    end

    pen += pen_contrib

    return pen, mds
end

################################################################################
               ####    ##   #       ####          ###### # #####
              #    #  #  #  #      #    #         #      #   #
              #      #    # #      #              #####  #   #
              #      ###### #      #              #      #   #
              #    # #    # #      #    #         #      #   #
               ####  #    # ######  ####          #      #   #
                                          #######
################################################################################

# There are several strategies that can be chosen here. But we have found that
# the best way is to have the search continous, and then have calc_fit round to
# integers. So we defined the search space to be continuos (-1 in all dimdigits)
parsed_args["searchspace"] = RectSearchSpace(
    [(-parsed_args["distance"],parsed_args["distance"]) for _ in 1:(N^2) ],
    dimdigits = [-1 for _ in 1:(N^2) ])

"""
    fit_from_pen(pens[, w=w])

Given the penalties `pens`, the weighted fitness is computed (weights: `w`).
"""
function fit_from_pen(pens, w=w)
    return w' * pens
end

"""
    calc_fit(x; verbose, save)

Calculates fitness associated to input `x`, a N^2 dimensional vector/array, that
is reshaped into the NxN flux-matrix.
"""
function calc_fit(x; verbose=false, save=true, detailed=false, spider=spider)

    #        mmmm    m mm   mmm   mmmm    mmm    m mm   mmm
    #        #" "#   #"  " #"  #  #" "#      #   #"  " #"  #
    #        #   #   #     #""""  #   #  m"""#   #     #""""
    #        ##m#"   #     "#mm"  ##m#"  "mm"#   #     "#mm"
    #        #                    #
    #        "                    "

    # Format the matrix:
    n_mat = fix_matrix(x)

    # Calculate the matrix combination:
    tmat = get_matrix(n_mat)

    # calculate the tadpole
    tadpole = get_tadpole(tmat)

    # Calculate the characteristic polynomial
    cp = unitpoly()
    if typeof(tmat) == Array{Int64,2}
        cp = short_charpoly(tmat)
    end
    prs = factor(cp) |> fc -> Tuple{fmpz_poly,Int64}[ (p,m) for (p,m) in fc ] |>
        x -> sort(x; lt = (a,b) -> length(a[1]) < length(b[1]))

    # Calculate the eigenvalues:
    egen = eigvals(tmat)

    # Separate out real and complex eigenvalues
    real_egen = real.(filter(isreal, egen))
    cplx_egen = filter(x -> !isreal(x), egen)

    #                                   ""#      m      "
    #       mmmm    mmm   m mm    mmm     #    mm#mm  mmm     mmm    mmm
    #       #" "#  #"  #  #"  #      #    #      #      #    #"  #  #   "
    #       #   #  #""""  #   #  m"""#    #      #      #    #""""   """m
    #       ##m#"  "#mm"  #   #  "mm"#    "mm    "mm  mm#mm  "#mm"  "mmm"
    #       #
    #       "

    # Penalties directly associated with eigenvalues
    cplx_pen = cplx_penalty(cplx_egen)

    (spider && !isnzero(cplx_pen)) && return (cplx_pen, (cplx_pen, 0, 0, 0, 0, 0), tadpole)

    nc_pen = nc_penalty(cplx_egen)

    (spider && !isnzero(nc_pen)) && return (nc_pen, (0, nc_pen, 0, 0, 0, 0), tadpole)

    neg_pen = neg_penalty(real_egen)

    (spider && !isnzero(neg_pen)) && return (neg_pen, (0, 0, neg_pen, 0, 0, 0), tadpole)

    # Penalty for the tadpole
    tad_pen = tad_penalty(tadpole)

    diag_pen = 0
    diff_pen = 0

    # zero penalty and
    zero_pen, mds = zero_penalty(egen, tmat, prs, false)

    (spider && !isnzero(zero_pen)) && return (zero_pen, (0, 0, 0, 0, 0, zero_pen), tadpole)

    tmat2 = get_second_matrix(n_mat)

    zero_pen2, mds2 = zero_penalty(egen, tmat2, prs, do_earlystop(zero_pen))
    zero_pen += zero_pen2

    (spider && !isnzero(zero_pen)) && return (zero_pen, (0, 0, 0, 0, 0, zero_pen), tadpole)

    # If the matrix is not definitely diagonalisable, we have to check
    # diagonalisability and such
    if !is_def_diag(prs)

        evecs = zeros(Float64,0,0)

        # we check the diagonalisability for the first
        if length(mds) > 0
            if any(mds .≠ 0)
                diag_pen += 10
            end
        end

        (spider && !isnzero(diag_pen)) && return (diag_pen, (0, 0, 0, 0, diag_pen, 0), tadpole)

        # then diff_pen
        group = Array{Float64}[]
        if diag_pen == 0
            group = mkgroup(prs, egen)
            evecs = mkzero.(eigvecs(tmat))
            diff_pen += diff_penalty(group, evecs)
        end

        (spider && !isnzero(diff_pen)) && return (diff_pen, (0, 0, 0, diff_pen, 0, 0), tadpole)

        if length(mds2) > 0
            if any(mds2 .≠ 0)
                diag_pen += 10
            end
        end

        (spider && !isnzero(diag_pen)) && return (diag_pen, (0, 0, 0, 0, diag_pen, 0), tadpole)

    end

    #                              #
    #         m   m   mmm    m mm  #mmm    mmm    mmm    mmm
    #         "m m"  #"  #   #"  " #" "#  #   #  #   "  #"  #
    #          #m#   #""""   #     #   #  #   #   """m  #""""
    #           #    "#mm"   #     ##m#"  "#m#"  "mmm"  "#mm"

    if verbose
        println("The Matrix:")
        println(n_mat)
        println("The matrix in Mathematica notation:")
        println(mathematica_matrix(n_mat))
        println("The eigenvalues:")
        println(egen)
        println("The characteristic polynomial:")
        println(cp)
        println("and its factorization:")
        for (pr, m) in factor(cp)
            println("multiplicity: $m; poly: $pr")
        end
        println("The tadpole:")
        println(tadpole)
        println("cplx_pen: $cplx_pen")
        println("nc_pen: $nc_pen")
        println("neg_pen: $neg_pen")
        println("tad_pen: $tad_pen")
        println("diff_pen: $diff_pen")
        println("diag_pen: $diag_pen")
        println("zero_pen: $zero_pen")
    end


    #                      ""#                    m""    "      m
    #         mmm    mmm     #     mmm          mm#mm  mmm    mm#mm
    #        #"  "      #    #    #"  "           #      #      #
    #        #      m"""#    #    #               #      #      #
    #        "#mm"  "mm"#    "mm  "#mm"           #    mm#mm    "mm
    #
    #                                   """"""

    # Calculate fitness

    fit = fit_from_pen([
        cplx_pen,
        nc_pen,
        neg_pen,
        tad_pen,
        diff_pen,
        diag_pen,
        zero_pen
    ])

    if !detailed
        return fit
    else
        # Notice: tad_pen not included
        return (fit, (cplx_pen, nc_pen, neg_pen, diff_pen, diag_pen, zero_pen), tadpole)
    end
end
