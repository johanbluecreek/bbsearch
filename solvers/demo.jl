
# Defining a search space
#=
First we need to define the space over which solutions are searched in. This
means the dimension of the search space and the size and "resolution" of each
dimension.

To demonstrate the flexibility of BBO and therefore bbsearch, we will here
define a search space that is twelve dimensional all of them "continuous". The
first six dimensions are allowed to take values between (-20.0,20.0), but can be
adjusted dynamically when calling bbsearch by the option `-d` (see README and
help message for more information). The last six parameters goes between
(-5.0,5.0).
=#
scale = parsed_args["distance"]

parsed_args["searchspace"] = RectSearchSpace(
    [
        (-20.0*scale,20.0*scale), (-20.0*scale,20.0*scale),
        (-20.0*scale,20.0*scale), (-20.0*scale,20.0*scale),
        (-20.0*scale,20.0*scale), (-20.0*scale,20.0*scale),
        (-5.0,5.0), (-5.0,5.0), (-5.0,5.0),
        (-5.0,5.0), (-5.0,5.0), (-5.0,5.0)
    ]
)

# Defining the calc_fit function
"""
    calc_fit()

The "demo" calc_fit.

This demo solves a version of the multidimensional Rosenbrock function [1].

[1] https://en.wikipedia.org/wiki/Rosenbrock_function
"""
function calc_fit(x; verbose=false)

    # The functions we aim to minimize is:
    fit = sum(
        100*(x[2i-1]^2 - x[2i])^2 + (x[2i-1] - 1)^2
    for i=1:trunc(Int, length(x)/2))

    if verbose
        println("""
        This is the verbose message printed by bbsearch at the end of a run.

        The input was:

        $x

        and the resulting fitness for this input is:

        $fit
        """)
    end

    # returns fitness value
    fit
end
