#!/usr/bin/env julia

using BlackBoxOptim

const progname = "save_to_mathematica.jl"

help = """
usage: $progname [path to save-file]

Saves '[path to filename]_math' text file with a Mathematica List[] of
Mathematica formatted matrices. Can be read by Mathematica by e.g.

 mats = Flatten[Get /@ FileNames["[path to]/save_*_math"], 1];

where '[path to]' has to be replaced. Map onto several files to generate several
files at once with e.g.

 \$ find ./ -type f -name "save_[date]*_??" -exec [path to]/$progname '{}' \\;

where '[date]' and '[path to]' have to be replaced.

To truncate to a ceratain tadpole, and to remove all matrices that does not have
all penalties but the tadpole solve, use e.g.

 \$ [path to]/spider_search.jl [filename] 0 30

and '[filename]_spider' will contain matrices with tadpole '30' or below, which
then can be parsed by 'save30_to_mathematica.jl'.
"""

if length(ARGS) < 1
    println(help);
    exit();
end

filename = ARGS[1];

if !isfile(filename)
    println("ERROR: '$filename' is not a file. First argument must be filename of save file.")
    println(help)
    exit()
end

dir = String(@__DIR__)

# Which solver-file does not matter, fitness are not calculated
solverfile = dir * "/../minitad_common.jl"

println("Loading solver...")
include(solverfile)

println("Loading utility file...")

utilfile = dir * "/minitad_utils/spider_utils.jl"
include(utilfile)

println("Reading & parsing save file...")

ret = load_pop(filename)

saves = unique(ret)

if length(saves) > 0
    println("Saving $(length(saves)) matrices to file...")
    open(filename * "_math", "w") do f
        write(f, "{")
        for s in saves[1:end-1]
            write(f, mathematica_matrix(s))
            write(f, ",\n")
        end
        write(f, mathematica_matrix(saves[end]))
        write(f, "}")
    end
else
    println("WARNING: '$filename' appears to contain no solution worth looking at. Exiting.")
end
