# Minitad utility tools

[[_TOC_]]

## The Spider

The Spider is a "local search", that we supplement the DE searches with. The steps of this algorithm are

  * filter: remove all matrices that does not have all parts of the fitness solved except the tadpole
  * create: For every zero entry in every matrix, create new two matrices where that zero is `±1`. When generated, all matrices are filtered again.
  * kill: For all matrices, for all non-zero entries the entry is stepped one integer closer to zero. When all matrices are generated they are filtered again.

In the file `spider_utils.jl` you find the common functions for the other spider-files.

### `spider_infy.jl`

This script applies the above algorithm to matrices in batches, and the latest and lowest tadpole matrices are processed first. New batches are processed until a lowest protected tadpole is reached and the save file has reached its limit. For more information, see the help message:

```
$ ./spider_infy.jl
```

### `spider_merge.jl`

This script is for merging save files together. This can be from bbsearch, another spider-search, or exported from Mathematica. This is mainly used for merging together all save files from all the cuts of a search from bbsearch. For more information, see the help message:

```
$ ./spider_merge.jl
```

### `spider_search.jl`

The functionality of this search is mostly obsolete in favour of `spider_infy.jl`, but it is a handy tool to truncate a single file to filtered matrices and cut them to a highest matrix, e.g.:

```
$ ./spider_search.jl savefile 0 30
```

will save a new file `savefile_spider` where all matrices have all parts of the fitness solved except the tadpole, and the highest tadpole is `30`. For more information, see the help message:

```
$ ./spider_search.jl
```

## `save_to_mathematica.jl`

This creates a new plain-text file of a save-file (from spider or from bbsearch), that contains a Mathematica list of the matrices. For more information, see the help message:

```
$ ./save_to_mathematica.jl
```

This is useful for post-search processing of the matrices, as e.g. `minitad_common.jl` can produce false-positives (matrices that has apparent zero fitness (up to tadpole), but not actually).

## `benchamrkit.jl`

Development tool for running before and after changes are made to the fitness function to see performance gains/losses. At first run it generates `1000` matrices, and those same matrices are then used for future benchmarks.

## `test_minitad.py`

Development tool that runs all solvers with `minitad` in the name. Useful for making sure that no edit has been made that breaks one of the solvers.

## `plot_search.jl`

Mostly an unsupported script, but contains example code for showing how saves can be processed and used to plot various data from the search.