#!/usr/bin/env julia

const spider=true

################################################################################
                  #     #
                  #     # ######   ##   #####  ###### #####
                  #     # #       #  #  #    # #      #    #
                  ####### #####  #    # #    # #####  #    #
                  #     # #      ###### #    # #      #####
                  #     # #      #    # #    # #      #   #
                  #     # ###### #    # #####  ###### #    #
################################################################################

const progname = "spider_search.jl"

help = """
usage: $progname savefile depth [limit] [solver]

Searches for adjecent "good" matrices by turning on (to ±1) or off (to 0)
entries.

The argument `depth` will set the amount of steps the algorithm will take
(warning: probably exponential growth), zero allowed, and the option `limit`
sets the highest tadpole to be kept (default: 30).

The `solver` argument will choose which solver file to use (default:
'minitad_common.jl'). The file must be present in the 'solvers' directory of
the repository.

The output file will have the name `savefile` with a "_spider" appended to the
end. Unless that file already exists, then that file will be moved to a file
with the name `savefile` appended with "_spider_backup".

Examples:
depth=2, limit=30, takes two steps, dropping all matrices with tadpoles higher
  than 30 at every step.
depth=0, limit=24, takes no steps and simply truncates the input matrices to
  those with tadpole 24 or lower.

This also means one can use this script to parse out only the "good" matrices,
which is useful in combination with save_to_mathematica.jl and spider_merge.jl.
"""


#    mmmm    mmm    m mm   mmm    mmm           mmm    m mm   mmmm   mmm
#    #" "#      #   #"  " #      #"  #             #   #"  " #" "#  #
#    #   #  m"""#   #      """m  #""""         m"""#   #     #   #   """m
#    ##m#"  "mm"#   #     "mmm"  "#mm"         "mm"#   #     "#m"#  "mmm"
#    #                                                        m  #
#    "                                                         ""

if length(ARGS) < 2
    println(help)
    exit()
end

filename = ARGS[1]

if !isfile(filename)
    println("$progname: ERROR: '$filename' is not a file. First argument must be a savefile returned by bbsearch.jl, spider_search.jl, or be a save30 file.")
    println(help)
    exit()
end

depth = try
    Int(Meta.parse(ARGS[2]))
catch
    println("$progname: ERROR: '$(ARGS[2])' could not be converted to an integer. Second argument must be the numbers of steps you wish to take.")
    println(help)
    exit()
end

limit = try
    Float64(Meta.parse(ARGS[3]))
catch
    println("$progname: Info: No upper limit for the size of the tadpole set, going with 30.");
    30.0
end

solver = try
    ARGS[4]
catch
    solver_default = "minitad_common.jl";
    println("$progname: Info: No solver selected, going with: $solver_default.");
    solver_default
end

################################################################################
                            #     #
                            ##   ##   ##   # #    #
                            # # # #  #  #  # ##   #
                            #  #  # #    # # # #  #
                            #     # ###### # #  # #
                            #     # #    # # #   ##
                            #     # #    # # #    #
################################################################################

function main(filename=filename, depth=depth, limit=limit, good_cache=good_cache, bad_cache=bad_cache)

    good_pop = load_good_pop(filename)

    # make sure there is something to start with
    if length(good_pop) == 0
        println("$progname: ERROR: There were no valid good solutions in '$filename' to start with.")
        exit()
    end

    # start stepping
    s = good_pop
    steps = Array{Array{Int64,2},1}[s]
    print_status(union(steps...))

    if isfile(filename * "_spider")
        println("Warning: Save file '$(filename * "_spider")' exists. Moving to '$(filename * "_spider_backup")' (even if that exists.)")
        mv(filename * "_spider", filename * "_spider_backup", force=true)
    end

    open(filename * "_spider", "w") do f
        serialize(f, union(steps...))
    end

    for i in 1:depth
        s = step(setdiff(s, steps[1:end-1]...), limit, good_cache, bad_cache)
        push!(steps, s)

        print_status(union(steps...))

        open(filename * "_spider", "w") do f
            serialize(f, union(steps...))
        end

    end

end

#     #                 #
#     #mmm    mmm    mmm#  m   m
#     #" "#  #" "#  #" "#  "m m"
#     #   #  #   #  #   #   #m#
#     ##m#"  "#m#"  "#m##   "#
#                           m"
#                          ""


# load solver with e8roots-file and load utils

dir = String(@__DIR__)

solverfile = dir * "/../" * solver
include(solverfile)

utilfile = dir * "/minitad_utils/spider_utils.jl"
include(utilfile)

# fitness

nfit(x) = calc_fit(x, verbose=false, save=false, detailed=true)

# start cache

good_cache = Dict{UInt64,Float64}()
bad_cache = Dict{UInt64,Float64}()
done_cache = Dict{UInt64,Float64}()

# run main

main(filename, depth, limit, good_cache, bad_cache)
