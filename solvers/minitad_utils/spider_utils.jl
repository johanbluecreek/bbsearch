#!/usr/bin/env julia

using ProgressMeter
using UnicodePlots

################################################################################
           #######
           #       #    # #    #  ####  ##### #  ####  #    #  ####
           #       #    # ##   # #    #   #   # #    # ##   # #
           #####   #    # # #  # #        #   # #    # # #  #  ####
           #       #    # #  # # #        #   # #    # #  # #      #
           #       #    # #   ## #    #   #   # #    # #   ## #    #
           #        ####  #    #  ####    #   #  ####  #    #  ####
################################################################################

"""
    prompt_mintad(maxtad, mintad)

Prompts user to enter a new `mintad` or exit.
"""
function prompt_mintad(maxtad, mintad)
    while maxtad < mintad
        println("There are solutions with tadpoles lower than `mintad`=$mintad that are going to be removed. Plese enter a new `mintad`, lower or equal to $maxtad to continue.")
        print("New `mintad` (negative to exit): ")
        mintad = try
            Int(Meta.parse(chomp(readline())))
        catch
            println("Something wrong in the input given. Please try again.");
            0
        end
        if mintad < 0
            exit()
        end
    end
    return mintad
end

"""
    prompt_maxsize(total, maxsize)

Prompts user to enter a new `maxsize` or exit.
"""
function prompt_maxsize(total, maxsize)
    while total > maxsize
        println("There are more matrices for the lowest tadpole ($total) than allowed ($maxsize). Please enter a new `maxsize` that is larger than $total to continue.")
        print("New `maxsize` (negative to exit): ")
        maxsize = try
            Int(Meta.parse(chomp(readline())))
        catch
            println("Something wrong in the input given. Please try again.");
            0
        end
        if maxsize < 0
            exit()
        end
    end
    return maxsize
end

"""
    get_maxtad_and_total(mats, maxsize)

Returns `(maxtad, total)` where `maxtad` is the largest tadpole such that the
there are fewer than `maxsize` matrices with that tadpole or lower. Total is the
number of matrices that would be left if all matrices above `maxtad` were
removed.

Note that if `total` is larger than `maxsize` this means that `maxtad` is the
lowest tadpole available, and that those are too many according to `maxsize`.
"""
function get_maxtad_and_total(mats, maxsize)
    tal = tally(mats)
    it = 1
    maxtad = tal[it][1]
    total = tal[it][2]
    while total ≤ maxsize && it < length(tal)
        it += 1
        if total + tal[it][2] ≤ maxsize
            # if we can add more matrices, update largest tadpole possible and
            # the total number of matrices
            maxtad = tal[it][1]
            total += tal[it][2]
        else
            # if cannot add more, exit the while
            it += length(tal)
        end
    end
    return maxtad, total
end


"""
    direct_tadpole()

Give the tadpole directly without trying to figure it out through the fitness.
"""
function direct_tadpole(mat)
    get_tadpole(get_matrix(mat))
end

"""
    tally(mats)

Tallys the tadpoles values for the matrices.
"""
function tally(mats, good_cache=good_cache, bad_cache=bad_cache)
    (length(mats) == 0) && return []
    tads = round.(fast_tad.(mats), digits=1)
    tally = [ (bin, length(filter(x -> x == bin, tads))) for bin in min(tads...):max(tads...)]
    tally = [ t for t in tally if t[2] > 0 ]
    return tally
end

"""
    plotit(mats[, title])

Plots a tally of the tadpole/multiplicities as a horizontal bar-graph.
"""
function plotit(mats, title="Stored Matrices:")
    tall = tally(mats)
    if length(tall) > 15
        tall = [[ t for t in tall[1:10] ]; [ ("$(tall[11][1])-$(tall[end][1])", sum(tall[i][2] for i in 11:length(tall))) ]]
    end
    try
        barplot(
            reverse([t[1] for t in tall]),
            reverse([t[2] for t in tall]),
            xlabel="multiplicity", ylabel="tadpole", title=title
        )
    catch e
        println("Failed with: $e")
        println("len maths: $(length(mats))")
        println("tally: $tall")
        println("t[1]s: $(reverse([t[1] for t in tall]))")
        println("t[2]s: $(reverse([t[1] for t in tall]))")
    end
end

"""
    print_status(mats[, met=:store])

Prints a nice little message and graph for the matrices provided.
"""
function print_status(mats, met=:store, good_cache=good_cache, bad_cache=bad_cache)
    if met == :store
        println("""
        Currently there are $(length(mats)) matrices stored:

        $(plotit(mats))
        """)
    elseif met == :proc
        println("""
        Currently there are $(length(mats)) matrices about to be processed: $(tally(mats, good_cache, bad_cache))
        """)
    elseif met == :filter
        println("""
        Currently there are $(length(mats)) matrices that have not been processed:

        $(plotit(mats, "Left to Process:"))
        """)
    elseif met == :found
        println("""
        There were $(length(mats)) matrices found:

        $(plotit(mats, "Found:"))
        """)
    end
end

"""
    fast_tad(mat)

Returns the cached tadpole of the matrix `mat`.
"""
function fast_tad(mat, good_cache=good_cache, bad_cache=bad_cache)
    h = hash(mat)
    if haskey(good_cache, h)
        return good_cache[h]
    elseif haskey(bad_cache, h)
        return bad_cache[h]
    else
        return direct_tadpole(mat)
    end
end

"""
    remove_large(mats, limit)

Removes matrices from `mats` that has tadpole larger than `limit`.
"""
function remove_large(mats::Array{Array{Int64,2},1}, limit)::Array{Array{Int64,2},1}
    filter(m -> direct_tadpole(m) ≤ limit, mats)
end

"""
    filter_calculated(mats, cache)

Filters out and returns only those matrices that has been calculated and is in
`cache`.
"""
function filter_calculated(mats, cache)
    filter(x -> haskey(cache, hash(x)), mats)
end

"""
    filter_good(mats, limit[, maxtad, good_cache, bad_cache, p])

Using the cache, this returns only the matrices in `mats` that has all penalties
except the tadpole solved. `p` is a symbol that can be `:on` or `:off` to
display progress.
"""
function filter_good(mats, limit, maxtad = Inf, good_cache=good_cache, bad_cache=bad_cache, p=:off)::Array{Array{Int64,2},1}

    # filter out matrices that we do not need to calculate penalties for again
    # those with zero penalties
    good = filter_calculated(mats, good_cache)
    # and those with non-zero penalties
    bad = filter_calculated(mats, bad_cache)

    # Find the good ones among the matrices where penalties still have to be
    # calculated.
    goods = []
    if p == :off
        for m in setdiff(mats, good, bad)
            tad = fast_tad(m, good_cache, bad_cache)
            if tad ≤ maxtad
                ret = nfit(m)
            else
                ret = (Inf, (Inf,Inf), tad)
            end
            if all(isnzero.(ret[2]))
                push!(goods, m)
                good_cache[hash(m)] = ret[3]
            else
                bad_cache[hash(m)] = ret[3]
            end
        end
    elseif p == :on
        @showprogress "Computing fitness..." for m in setdiff(mats, good, bad)
            tad = fast_tad(m, good_cache, bad_cache)
            if tad ≤ maxtad
                ret = nfit(m)
            else
                ret = (Inf, (Inf,Inf), tad)
            end
            if all(isnzero.(ret[2]))
                push!(goods, m)
                good_cache[hash(m)] = ret[3]
            else
                bad_cache[hash(m)] = ret[3]
            end
        end
    end

    return remove_large(Array{Int64,2}[good; goods], limit)
end

"""
    load_good_pop(filename)

Opens the file given to the spider and guesses the type of save-file from the
type. Only "good" solutions are then filtered from the full population.
"""
function load_good_pop(filename, p=:on, limit=limit, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache)
    return filter_good(load_pop(filename), limit, maxtad, good_cache, bad_cache, p)
end

"""
    load_pop(filename)

Opens the file given to the spider and guesses the type of save-file from the
type.
"""
function load_pop(filename)
    # open save
    ret = open(filename, "r") do sf
        deserialize(sf)
    end

    good_pop = Array{Int64,2}[]
    if typeof(ret) <: Tuple{Any,Any,Any} # guessing it is a bbsearch save

        # parse out the components
        opt, res, parsed_args_old  = ret
        # get the population
        pop = res.method_output.population.individuals
        N = Int64(sqrt(size(pop,1)))
        pop = [ pop[:,i] for i in 1:Int(length(pop)//(N^2)) ];
        # TODO: If you get an InexactError in the above, that is because this script does not yet support minitad_continue.

        # get the good population
        good_pop = map(x -> reshape(round.(Int,x), (N,N)), pop)

    elseif typeof(ret) == Array{Array{Int64,2},1} # guessing it is an old spider search

        good_pop = ret

    elseif typeof(ret) == Array{Array{Float64,1},1} # guessing it is a save30

        N = length(first(ret))
        good_pop = map(x -> reshape(round.(Int,x), (N,N)), ret)

    elseif typeof(ret) == Symbol # guessing it is a text-file from Mathematica

        txt = open(filename, "r") do sf
            read(sf, String)
        end
        println("$progname: Info: You appear to have loaded a file that is exported from Mathematica. Julia will attempt to parse this into Julia expressions which may take time. Be patient.")
        ret = map(t -> collect(hcat(eval(Meta.parse(t))...)'), split(replace(replace(txt, "{" => "["), "}" => "]"), "\n"))
        good_pop = ret

    else

        println("$progname: ERROR: Could not figure out type of save file this was. This one returned something with type '$(typeof(ret))'.")
        exit()

    end

    return good_pop
end

#                                    m
#       mmm    m mm   mmm    mmm   mm#mm   mmm
#      #"  "   #"  " #"  #      #    #    #"  #
#      #       #     #""""  m"""#    #    #""""
#      "#mm"   #     "#mm"  "mm"#    "mm  "#mm"

"""
    zero_entries(mat)

Returns the indices of all zero entries of the input matrix `mat`.
"""
zero_entries(mat) = findall(iszero, mat)

"""
    nonzero_generate(mat)

Generates nonzero entries (±1) where `mat` has a zero entry, and returns all
those matrices.
"""
function nonzero_generate(mat)::Array{Array{Int64,2},1}
    [
        begin
            a = copy(mat); a[entry] = 1
            b = copy(mat); b[entry] = -1
            (a, b)
        end
    for entry in zero_entries(mat)] |> Iterators.flatten |> collect
end

"""
    create(mat, limit)

Retuns all matrices with all penalties except the tadpole zero that has been
generated by creating ±1s where there before were zeroes. The input `mat` can be
a list of matrices.
"""
function create(mat::Array{Int64,2}, limit, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache)::Array{Array{Int64,2},1}
    filter_good(nonzero_generate(mat), limit, maxtad, good_cache, bad_cache)
end

function create(mats::Array{Array{Int64,2},1}, limit, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache)::Array{Array{Int64,2},1}
    create.(mats, limit, maxtad, good_cache, bad_cache) |> Iterators.flatten |> collect
end

#      #        "    ""#    ""#
#      #   m  mmm      #      #
#      # m"     #      #      #
#      #"#      #      #      #
#      #  "m  mm#mm    "mm    "mm

"""
    nonzero_entries(mat)

Returns the indices of all non-zero entries of the input matrix `mat`.
"""
nonzero_entries(mat) = findall(!iszero, mat)

"""
    zero_generate(mats, mat)

Generates entries closer to zero on all the matrices of `mats` where `mat` has
a non-zero entry, and returns all those matrices.
"""
function zero_generate(mats, mat)
    [
        (
            begin a = copy(m); a[entry] > 0 ? a[entry] -= 1 : a[entry] += 1 ; a end
        for entry in nonzero_entries(mat))
    for m in mats] |> Iterators.flatten |> collect
end

"""
    kill(mats, mat, limit)

Retuns all matrices with all penalties except the tadpole zero that has been
generated by killing ±1s.
"""
function kill(mats, mat, limit, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache)
    filter_good(zero_generate(union(mats,[mat]), mat), limit, maxtad, good_cache, bad_cache)
end

#               m
#       mmm   mm#mm   mmm   mmmm
#      #        #    #"  #  #" "#
#       """m    #    #""""  #   #
#      "mmm"    "mm  "#mm"  ##m#"
#                           #
#                           "

# for spider_search

function step(mat::Array{Int64,2}, limit=3000, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache, done_cache=done_cache, p=Progress(length(m); desc="Taking steps: "))::Array{Array{Int64,2},1}
    created = create(mat, limit, Inf, good_cache, bad_cache)
    killed = kill(created, mat, limit, Inf, good_cache, bad_cache)
    done_cache[hash(mat)] = fast_tad(mat, good_cache, bad_cache)
    next!(p)
    return union(created, killed)
end

function step(mats::Array{Array{Int64,2},1}, limit=3000, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache, done_cache=done_cache)::Array{Array{Int64,2},1}
    p = Progress(length(mats); desc="Taking steps: ")
    ProgressMeter.update!(p, 0)
    union(map(x -> step(x, limit, maxtad, good_cache, bad_cache, done_cache, p), mats)...)
end

# for spider_infy

function step(mat::Array{Int64,2}, limit=3000, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache, done_cache=done_cache, p=Progress(length(m); desc="Taking steps: "))::Array{Array{Int64,2},1}
    created = create(mat, limit, Inf, good_cache, bad_cache)
    killed = kill(created, mat, limit, maxtad, good_cache, bad_cache)
    done_cache[hash(mat)] = fast_tad(mat, good_cache, bad_cache)
    next!(p)
    return union(created, killed)
end

function step(mats::Array{Array{Int64,2},1}, limit=3000, maxtad=Inf, good_cache=good_cache, bad_cache=bad_cache, done_cache=done_cache)::Array{Array{Int64,2},1}
    p = Progress(length(mats); desc="Taking steps: ")
    ProgressMeter.update!(p, 0)
    union(map(x -> step(x, limit, maxtad, good_cache, bad_cache, done_cache, p), mats)...)
end

# doc

@doc """
    step(mats, limit, good_cache, bad_cache, [done_cache], p)

Takes a combined `create()` and `kill()` step and showing a progress bar.
""" step
