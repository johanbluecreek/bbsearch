#!/usr/bin/env julia

using BenchmarkTools

help = """
usage: ./benchmarkit.jl

Generates 1000 matrices and runs a timed benchmark on them. The matrices are
only generated on the first run, so that the benchmark can be repeated on the
same matrices, to compare performance gain/losses when developing.
"""

if length(ARGS) > 0
    println(help)
    exit()
end

dir = String(@__DIR__)

bmdir = dir * ""

solverfile = bmdir * "/../minitad_common.jl"
bmfile = bmdir * "/bm_matrices"

include(solverfile)

NMATS = 1000

mats = [ ]

if isfile(bmfile)
    println("benchmarkit.jl: Info: Loading matrices...")
    mats = open(bmfile, "r") do sf
        deserialize(sf)
    end
    NMATS = length(mats)
else
    println("benchmarkit.jl: Info: Generating and saving matrices...")
    mats = [ randn(N^2)/4 for _ in 1:NMATS ]
    open(bmfile, "w") do sf
        serialize(sf, mats)
    end
end

println("benchmarkit.jl: Info: Executing benchmark ($NMATS matrices)...")

@btime calc_fit.(m) setup=(m=mats)
