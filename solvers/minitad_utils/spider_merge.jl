#!/usr/bin/env julia

const spider=true

################################################################################
                  #     #
                  #     # ######   ##   #####  ###### #####
                  #     # #       #  #  #    # #      #    #
                  ####### #####  #    # #    # #####  #    #
                  #     # #      ###### #    # #      #####
                  #     # #      #    # #    # #      #   #
                  #     # ###### #    # #####  ###### #    #
################################################################################

using StatsBase
using ProgressMeter

const progname = "spider_merge.jl"

help = """
usage: $progname maxtad solver savefiles

Merges files into one, and truncates to `maxtad` and lower tadpoles. You must
also specify `solver` to indicate which file to use for computing fitness (give
any file present in the 'solvers/' folder of the repository).

The merged files will be named 'spider_merge_[hash]', where the number '[hash]'
will be based on the file names, hence re-running the program on the same files
will produce the same output file.
"""


#    mmmm    mmm    m mm   mmm    mmm           mmm    m mm   mmmm   mmm
#    #" "#      #   #"  " #      #"  #             #   #"  " #" "#  #
#    #   #  m"""#   #      """m  #""""         m"""#   #     #   #   """m
#    ##m#"  "mm"#   #     "mmm"  "#mm"         "mm"#   #     "#m"#  "mmm"
#    #                                                        m  #
#    "                                                         ""

if length(ARGS) < 3
    println(help)
    exit()
end

maxtad = try
    Int(Meta.parse(ARGS[1]))
catch
    println("$progname: ERROR: No maximum tadpole given.");
    println(help)
    exit()
end

solver = try
    ARGS[2]
catch
    println("$progname: ERROR: No solver given.");
    println(help)
    exit()
end

filenames = ARGS[3:end]

for filename in filenames
    if !isfile(filename)
        println("$progname: ERROR: '$filename' is not a file. First argument must be a savefile returned by bbsearch.jl, spider_{search,infy}.jl, or be a save30 file.")
        println(help)
        exit()
    end
end

limit = 2000

################################################################################
                            #     #
                            ##   ##   ##   # #    #
                            # # # #  #  #  # ##   #
                            #  #  # #    # # # #  #
                            #     # ###### # #  # #
                            #     # #    # # #   ##
                            #     # #    # # #    #
################################################################################

function main(filenames=filenames, maxtad=maxtad)

    good_pops = []
    @showprogress 1 "Processing files..." for file in filenames
        append!(good_pops, [load_good_pop(file, :off)])
    end

    good_pop = union(good_pops...)

    # make sure there is something there
    if length(good_pop) == 0
        println("$progname: ERROR: There were no valid good solutions in any of the files.")
        exit()
    end

    s = good_pop
    print_status(s, :found)

    if tally(s)[end][1] > maxtad
        println("Truncating to tadpole: $maxtad")
        s = remove_large(s, maxtad)
        print_status(s, :found)
    end

    savefile = "spider_merge_$(hash(filenames))"

    if isfile(savefile)
        println("Warning: Save file '$savefile' exists. Moving to '$(savefile * "_backup")' (even if that exists.)")
        mv(savefile, savefile * "_backup", force=true)
    end

    open(savefile, "w") do f
        serialize(f, s)
    end

    println("Savefile is: '$savefile'")

end


#     #                 #
#     #mmm    mmm    mmm#  m   m
#     #" "#  #" "#  #" "#  "m m"
#     #   #  #   #  #   #   #m#
#     ##m#"  "#m#"  "#m##   "#
#                           m"
#                          ""


# load solver and load utils

dir = String(@__DIR__)

solverfile = dir * "/../" * solver
include(solverfile)

utilfile = dir * "/minitad_utils/spider_utils.jl"
include(utilfile)

# fitness

nfit(x) = calc_fit(x, verbose=false, save=false, detailed=true)

# start cache

good_cache = Dict{UInt64,Float64}()
bad_cache = Dict{UInt64,Float64}()
done_cache = Dict{UInt64,Float64}()

# run main

main(filenames, maxtad)
