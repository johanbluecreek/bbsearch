#!/usr/bin/env python3

import os
import subprocess

devnull = open(os.devnull, 'w')

if __name__ == '__main__':

    # This file path:
    file_path = os.path.dirname(os.path.realpath(__file__))

    # These are all solvers we want to test
    solvers = os.listdir(file_path + "/../")
    solvers = [ s for s in solvers if ".jl" in s and "minitad" in s]

    # Change working directory to where "bbsearch.jl" is
    os.chdir(file_path + "/../../")

    for solver in solvers:
        print("Running test for: {}".format(solver.strip(".jl")))
        # run bbsearch for every solver to make sure it works
        cmd = [
            "./bbsearch.jl",
            "-n",
            "-s", solver.strip(".jl"),
            "-r", "10.0",
            "-d", ".55",
            "--ignoregit"
        ]

        p = subprocess.Popen(
            cmd
        , shell=False
        #, stdout=devull
        )

        print("running...")
        p.communicate()
        print("done.")

        #print("Run exited with code: {}".format(p.returncode))
        if p.returncode != 0:
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            print("There was an error for: {}".format(solver.strip(".jl")))

        print("")
