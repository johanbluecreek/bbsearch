#!/usr/bin/env julia

################################################################################
                  #     #
                  #     # ######   ##   #####  ###### #####
                  #     # #       #  #  #    # #      #    #
                  ####### #####  #    # #    # #####  #    #
                  #     # #      ###### #    # #      #####
                  #     # #      #    # #    # #      #   #
                  #     # ###### #    # #####  ###### #    #
################################################################################

ENV["GKSwstype"]="100"
using Plots
using StatsBase
using Statistics
using ProgressMeter
using Measures

const progname = "plot_search.jl"

help = """
usage: $progname basename [store]

Generates a multitude of plots from a search sequence by considering all
`basename_?+` files. Add anything as a second argument to disable the script
saving populations and fitness values to a storage file (WARN: this file
takes as much space as all the savefiles together, so it may be large.)

You can also run this script on the `_plotstorage` files that are produced after
the first run.

If you wish to combine several runs with different cuts into one, then make sure
they have the same basename and that the "_[number]" ending are in sequence. The
spacing in graphs will be independent of actual cut-time.
"""


#    mmmm    mmm    m mm   mmm    mmm           mmm    m mm   mmmm   mmm
#    #" "#      #   #"  " #      #"  #             #   #"  " #" "#  #
#    #   #  m"""#   #      """m  #""""         m"""#   #     #   #   """m
#    ##m#"  "mm"#   #     "mmm"  "#mm"         "mm"#   #     "#m"#  "mmm"
#    #                                                        m  #
#    "                                                         ""

if length(ARGS) < 1
    println(help)
    exit()
end

const filename = ARGS[1]

if !isfile(filename * "_1") && !isfile(filename * "_plotstorage") && !isfile(filename)
    println("$progname: ERROR: First file in the sequence, '$(filename)_1', or storage file, '$(filename)_plotstorage', appears to not exist.")
    println(help)
    exit()
end

store = true
if length(ARGS) > 1
    println("Will not save to storage...")
    store = false
end

################################################################################
           #######
           #       #    # #    #  ####  ##### #  ####  #    #  ####
           #       #    # ##   # #    #   #   # #    # ##   # #
           #####   #    # # #  # #        #   # #    # # #  #  ####
           #       #    # #  # # #        #   # #    # #  # #      #
           #       #    # #   ## #    #   #   # #    # #   ## #    #
           #        ####  #    #  ####    #   #  ####  #    #  ####
################################################################################

"""
    fix_filenames()

Makes sure the filenames are sorted in the correct order.
"""
function fix_filenames(files)
    # do a little dance...
    reg = "_(\\d)"
    while !all(match.(Regex(reg * "\\d"), files) .=== nothing)
        files = replace.(files, Regex(reg * "(?!\\d)") => s"_0\1")
        reg = reg[1:end-1] * "\\d)"
    end
    # ... make a little sort...
    sort!(files)
    # ... get the file names right
    reg = "_0"*reg[2:end-3]*")"
    while length(reg) ≥ 6
        files = replace.(files, Regex(reg) => s"_\1")
        reg = reg[1:end-3]*")"
    end
    # that is, this adds zeroes until the sorting would turn out correctly,
    # then it sorted, and then removes all the zeroes again.
    return files
end

"""
    get_tadpole_weight()

Returns the tadpole weight.
"""
function get_tadpole_weight(w=w)
    return w[4]
end

"""
    get_matrix_dim()

Returns dimension of intersection matrix (`N`).
"""
function get_matrix_dim(N=N)
    return N
end

"""
    get_store()

Returns the global sore value.
"""
function get_store(store=store)
    return store
end

################################################################################
                            #     #
                            ##   ##   ##   # #    #
                            # # # #  #  #  # ##   #
                            #  #  # #    # # # #  #
                            #     # ###### # #  # #
                            #     # #    # # #   ##
                            #     # #    # # #    #
################################################################################

function main(filename=filename)

    println("Fixing filenames...")

    # split the name if it is an aboslute path
    dirname = Base.Filesystem.dirname(filename)
    if dirname == ""
        dirname = "./"
    else
        dirname = dirname * "/"
    end
    basename = Base.Filesystem.basename(filename)

    pops = Array{Float64,2}[]
    all_nfits = Array{Tuple{Float64,Tuple{Float64,Float64,Float64,Int64,Int64,Int64},Float64},1}[]

    if basename[end-11:end] == "_plotstorage"
        storagename = dirname * basename
        basename = basename[1:end-12]
    elseif basename[end-1:end] == "_1"
        storagename = dirname * basename[1:end-2] * "_plotstorage"
        basename = basename[1:end-2]
    else
        storagename = dirname * basename * "_plotstorage"
    end

    if isfile(storagename)
        println("File storage found, using that...")
        pops, all_nfits = open(storagename, "r") do sf
            deserialize(sf)
        end
    else
        # parse out all the relevant files
        strs = ["spider", "math", "png", "mp4"]
        files = [
            name
        for name in readdir(dirname) if occursin(basename, name) && all(.!occursin.(strs, name))]

        # The naming convention for the files are bad, so we need to do some work...
        files = fix_filenames(files)
        files = dirname .* files

        println("These are the files that will be processed, in order:")
        println.(files)

        # parse out all the populations
        pops = Array{Float64,2}[]
        for file in files
            opt, res, parsed_args_old = open(file, "r") do sf
                deserialize(sf)
            end
            if hasproperty(opt.optimizer, :population) # result from a DE
                push!(pops, opt.optimizer.population.individuals)
            elseif hasproperty(opt.optimizer, :best) # result from random_search
                push!(pops, hcat([opt.optimizer.best]...))
            else
                println("$progname: ERROR: Input file(s) appear to be unsupported. Will exit.")
                exit(4)
            end
        end

        # parse out all fitnesses
        p = Progress(length(pops); desc="Calculating fitness: ")
        all_nfits = [
            begin next!(p); [mapslices(nfit, pop, dims=1)...] end
            for pop in pops]
        # these are detailed fitnesses

        if get_store()
            println("Saving pops and fitness to storage...")
            # save these to store
            open(storagename, "w") do sf
                serialize(sf, (pops, all_nfits))
            end
        else
            println("Storage was disabled, not saving...")
        end
    end

    println("Parsing saves...")

    # Get the fitnesses, also separate in good and bads

    fitness_series = [
        [f[1] for f in nfits]
    for nfits in all_nfits]

    fitness_series_good = [
        [f[1] for f in nfits if all(isnzero.(f[2]))]
    for nfits in all_nfits]

    fitness_series_bad = setdiff(fitness_series, fitness_series_good)

    # Get tadpoles. also separate in good and bads

    tadpole_series = [
        [f[end] for f in nfits]
    for nfits in all_nfits]

    tadpole_series_good = [
        [f[end] for f in nfits if all(isnzero.(f[2]))]
    for nfits in all_nfits]

    tadpole_series_bad = setdiff(tadpole_series, tadpole_series_good)

    # When we are not plotting everything, then we plot first, last and 5 in
    # between, so use this range:
    rn = 2:floor(Int,length(pops)/5):length(pops)
    if length(pops) ∉ rn
        rn = [rn; length(pops)]
    end
    # this can mean some awkward spacing in the start or end, but thats OK.

    # The natural bin-size for fitness is tadpole weight, as this is the
    # separation at late stage searches
    fitness_bins = 0:get_tadpole_weight():max(vcat(fitness_series...)...)
    #fitness_bins_good = 0:get_tadpole_weight():max(vcat(fitness_series_good...)...)
    #fitness_bins_bad = 0:get_tadpole_weight():max(vcat(fitness_series_bad...)...)
    # while for tadpoles it's just integers
    tadpole_bins = 0:max(vcat(tadpole_series...)...)
    #tadpole_bins_good = 0:max(vcat(tadpole_series_good...)...)
    #tadpole_bins_bad = 0:max(vcat(tadpole_series_bad...)...)

    xticks = 0:10:length(pops)

    dist_est = (max(pops[1]...) - min(pops[1]...)) |> x -> round(x/2, digits=3)
    popsize = size(pops[1],2)
    extra_str = "\n(-d $dist_est,-p $popsize)"

    # We can now start plotting.
    println("Starting to plot...")

    # Plotting all fitnesses
    hist = histogram(fitness_series[1], bins=fitness_bins, legend=false, title="fitness evolution (all cuts, global min: $( round(min(min.(fitness_series...)...),sigdigits=2) ))" * extra_str)
    for f in fitness_series[2:end]
        histogram!(hist, f, bins=fitness_bins)
    end
    savefig(hist,dirname * basename * "_fit_dist_all.png")

    mp4(@animate(for i=1:length(fitness_series)
        histogram(fitness_series[i], bins=fitness_bins, legend=false, title="fitness evolution (cut: $i, current min: $( round(min(fitness_series[i]...),sigdigits=2) ))" * extra_str,ylim=(0,60))
    end), (dirname * basename * "_fit_dist_all.mp4"))

    # Plotting every 5 fitnesses
    hist = histogram(fitness_series[1], bins=fitness_bins, legend=false, title="fitness evolution (1st + last & every 5 cut, global min: $( round(min(min.(fitness_series...)...),sigdigits=2) ))" * extra_str)
    for f in fitness_series[rn]
        histogram!(hist, f, bins=fitness_bins)
    end
    savefig(hist,dirname * basename * "_fit_dist_every5.png")

    # Plotting all tadpoles
    hist = histogram(tadpole_series[1], bins=tadpole_bins, legend=false, title="tadpole evolution (all cuts, good global min: $(round(Int,min([ min(ts...) for ts in filter(x -> length(x) > 0, tadpole_series_good)]...))))" * extra_str)
    for f in tadpole_series[2:end]
        histogram!(hist, f, bins=tadpole_bins)
    end
    savefig(hist,dirname * basename * "_tad_dist_all.png")

    mp4(@animate(for i=1:length(tadpole_series)
        histogram(tadpole_series[i], bins=tadpole_bins, legend=false, title="tadpole evolution (cut: $i, current good min: $(tadpole_series_good[i] |> x -> length(x) > 0 ? round(Int,min(x...)) : NaN))" * extra_str,ylim=(0,60))
    end), (dirname * basename * "_tad_dist_all.mp4"))

    # Plotting every 5 tadpoles
    hist = histogram(tadpole_series[1], bins=tadpole_bins, legend=false, title="tadpole evolution (1st + last & every 5 cut, good global min: $(round(Int,min([ min(ts...) for ts in filter(x -> length(x) > 0, tadpole_series_good)]...))))" * extra_str)
    for f in tadpole_series[rn]
        histogram!(hist, f, bins=tadpole_bins)
    end
    savefig(hist,dirname * basename * "_tad_dist_every5.png")

    # Plotting improvement in fitness for last step
    #fitness_bins_trunc = length(fitness_bins) > 10 ? fitness_bins[1:10] : fitness_bins # these are just zero...
    hist = histogram(fitness_series[end-1], bins=fitness_bins, label="next to last", alpha=.7, title="comparison in fitness for two last cuts" * extra_str)
    histogram!(hist, fitness_series[end], bins=fitness_bins, label="last", alpha=.7)
    savefig(hist,dirname * basename * "_fit_dist_last2.png")

    # Plotting relative improvement in mean
    pl = plot(
        [
            mean(fitness_series[i]) ≠ 0 ? (mean(fitness_series[i]) - mean(fitness_series[i+1]))/mean(fitness_series[i]) : 0
        for i in 1:length(fitness_series[1:(end-1)])],
        title = "Improvement of relative mean" * extra_str, label = "(μ(i) - μ(i+1))/μ(i)", xticks=xticks
    )
    savefig(pl,dirname * basename * "_rel_mean.png")

    # Plotting absolue mean
    pl = plot(
        [
            mean(f)
        for f in fitness_series],
        title = "Evolution of mean" * extra_str, label = "μ(i)", xticks=xticks
    )
    savefig(pl,dirname * basename * "_abs_mean.png")

    # Plotting min fitness
    pl = plot(
        [
            min(f...)
        for f in fitness_series],
        title = "Evolution of min" * extra_str, label = "min(i)", xticks=xticks
    )
    savefig(pl,dirname * basename * "_abs_min.png")

    # Plotting comparative tadpole distribution of good and bad solution
    if length(tadpole_series_bad) > 0 && length(tadpole_series_good) > 0
        hist = histogram(tadpole_series_bad[end], bins=tadpole_bins, label="bad (min: $(round(Int,min([ min(ts...) for ts in filter(x -> length(x) > 0, tadpole_series_bad)]...))))", title="Tadpole distribution of good vs bad" * extra_str)
        histogram!(hist, tadpole_series_good[end], bins=tadpole_bins, label="good (min: $(round(Int,min([ min(ts...) for ts in filter(x -> length(x) > 0, tadpole_series_good)]...))))")
        savefig(hist,dirname * basename * "_tad_compare.png")
    end

    # Plot the diversity of the population by counting number of unique matrices (as integer matrices)
    #=
    pl = plot(
        [ length(unique([fix_matrix(pop[:,l]) for l in 1:size(pop,2)])) for pop in pops],
        title="Evolution of diversity" * extra_str, label="# of matrices", xticks=xticks
    )
    savefig(pl,dirname * basename * "_diversity.png")
    =#

    # Same plot again, but in percent of whole population (better for comparison)
    pl = plot(
        [ length(unique([fix_matrix(pop[:,l]) for l in 1:size(pop,2)]))/size(pop,2) for pop in pops],
        title="Evolution of diversity" * extra_str, label="% of unique matrices", xticks=xticks, yticks=0:0.05:1
    )
    savefig(pl,dirname * basename * "_diversity_rel.png")

    # All plots are misleading in that a late time spike may actually be just a single matrix, because we do not
    # take into account how diversity is killed off. So lets do the plots again, but by filtering out
    # degeneracies.
    fit_tad_pop = [
        [ (all_nfits[j][i][1], all_nfits[j][i][end], fix_matrix(pops[j][:,i])) for i in 1:length(all_nfits[j]) ] |> unique
    for j in 1:length(all_nfits)]

    fit_div_series = [
        [t[1] for t in ftp]
    for ftp in fit_tad_pop]
    fit_div_bin = 0:get_tadpole_weight():max(vcat(fit_div_series...)...)

    tad_div_series = [
        [t[2] for t in ftp]
    for ftp in fit_tad_pop]
    tad_div_bin = 0:max(vcat(tad_div_series...)...)

    mp4(@animate(for i=1:length(fit_div_series)
        histogram(fit_div_series[i], bins=fit_div_bin, legend=false, title="fitness evolution (cut: $i, unique, (#: $(length(fit_div_series[i])), min: $( round(min(fit_div_series[i]...),sigdigits=2) )))" * extra_str,ylim=(0,60))
    end), (dirname * basename * "_fit_dist_all_diversity.mp4"))

    mp4(@animate(for i=1:length(tad_div_series)
        histogram(tad_div_series[i], bins=tad_div_bin, legend=false, title="tadpole evolution (cut: $i, unique, (#: $(length(tad_div_series[i])), min: $( round(Int,min(tad_div_series[i]...)) )))" * extra_str,ylim=(0,60))
    end), (dirname * basename * "_tad_dist_all_diversity.mp4"))

    # We can also attempt to plot how entries become frozen during the search
    jump_numbers = [0; [ ([ pops[j][i,:] |> x -> round.(Int64,x) |> x -> max(x...) - min(x...) for i in 1:size(pops[1],1) ] |> x -> reshape(x, (N,N))) -
        ([ pops[j+1][i,:] |> x -> round.(Int64,x) |> x -> max(x...) - min(x...) for i in 1:size(pops[1],1) ] |> x -> reshape(x, (N,N))) |> sum
    for j in 1:length(pops)-1]]

    pl = jump_numbers |> x -> plot(x, xticks=xticks, title="Number of entries removed from population" * extra_str, legend=false)
    savefig(pl,dirname * basename * "_jumpnumbers.png")

    mp4(@animate(for j in 1:length(pops)
        [ pops[j][i,:] |> x -> round.(Int64,x) |> x -> max(x...) - min(x...) for i in 1:size(pops[1],1) ] |>
            x -> reshape(x, (N,N)) |>
            z -> [ (i,j,z[i,j]) for i in 1:size(z,1), j in 1:size(z,2)] |>
            z -> vcat(z...) |>
            z -> scatter3d(z, zlim=(0,ceil(Int,dist_est)*2-2), legend=false, title="Freezing of entries (cut: $j, jumps: $(jump_numbers[j]))" * extra_str)
    end), (dirname * basename * "_jumpnumbers.mp4"))

    println("Appears I'm done. Bye.")
    return nothing
end

#     #                 #
#     #mmm    mmm    mmm#  m   m
#     #" "#  #" "#  #" "#  "m m"
#     #   #  #   #  #   #   #m#
#     ##m#"  "#m#"  "#m##   "#
#                           m"
#                          ""

dir = String(@__DIR__)

e8rootsfile = dir * "/../../e8roots.txt"
cleanup = false
if !isfile("e8roots.txt")
    cp(e8rootsfile, "e8roots.txt")
    cleanup = true
else
    println("$progname: Warning: Existing 'e8roots.txt' file found in the current directory and will be used. If this is not up to date with bbsearch, vectors may be missed.")
end

#solverfile = dir * "/../minitad_common.jl"
solverfile = dir * "/../minitad_e8.jl"
include(solverfile)

if cleanup
    rm("e8roots.txt")
end

# fitness

nfit(x) = calc_fit(x, verbose=false, save=false, detailed=true)

# run main

main()
