#!/usr/bin/env julia

const spider=true

################################################################################
                  #     #
                  #     # ######   ##   #####  ###### #####
                  #     # #       #  #  #    # #      #    #
                  ####### #####  #    # #    # #####  #    #
                  #     # #      ###### #    # #      #####
                  #     # #      #    # #    # #      #   #
                  #     # ###### #    # #####  ###### #    #
################################################################################

using StatsBase

const progname = "spider_newinfy.jl"

help = """
usage: $progname savefile [mintad] [maxsize] [batchsize] [solver]

The option `mintad` is the minimum you want to reach (default: 24). This means,
the script will exit before removing any matrices of tadpole `mintad` or
smaller.

The `maxsize` option will set the maximum number of matrices to be saved in the
"-remaining" file. If a previous step reaches above this number the matrices
will be reduced to the maximum tadpole that brings down the number below
`maxsize`. If the matrices with the lowest tadpole are more than `maxsize` then
the program will halt, and ask the user if a new `maxsize` should be set, and if
you want to continue. (default: 200,000)

The option `batchsize` sets how large a batch of matrices should be processed at
once. Lower size makes each step faster, but creates larger overhead.

The `solver` option is a switch for which solver file to use, the default is
`minitad_common.jl`. Use just the file name, and the file has to be in `solvers/
minitad_utils/`.

The output file will have the name `savefile` with a "_spider" appended to the
end, and the file ending in a "-remain" are the matrices that has not yet
been processed, and "-processed" saves all the finished ones.
"""


#    mmmm    mmm    m mm   mmm    mmm           mmm    m mm   mmmm   mmm
#    #" "#      #   #"  " #      #"  #             #   #"  " #" "#  #
#    #   #  m"""#   #      """m  #""""         m"""#   #     #   #   """m
#    ##m#"  "mm"#   #     "mmm"  "#mm"         "mm"#   #     "#m"#  "mmm"
#    #                                                        m  #
#    "                                                         ""

if length(ARGS) < 1
    println(help)
    exit()
end

filename = ARGS[1]

if !isfile(filename)
    println("$progname: ERROR: '$filename' is not a file. First argument must be a savefile returned by bbsearch.jl, spider_{search,infy}.jl, or be a save30 file.")
    println(help)
    exit()
end

mintad = try
    Int(Meta.parse(ARGS[2]))
catch
    mintad_default = 24;
    println("$progname: Info: No protected tadpole set, going with $mintad_default.");
    mintad_default
end

maxsize = try
    Int(Meta.parse(ARGS[3]))
catch
    maxsize_default = 200_000;
    println("$progname: Info: No maximum number of matrices set, going with $maxsize_default.");
    maxsize_default
end

batchsize = try
    Int(Meta.parse(ARGS[4]))
catch
    batchsize_default = 50;
    println("$progname: Info: No upper limit for the batchsize set, going with $batchsize_default.");
    batchsize_default
end

solver = try
    ARGS[5]
catch
    solver_default = "minitad_common.jl";
    println("$progname: Info: No solver selected, going with: $solver_default.");
    solver_default
end

limit = 300000

################################################################################
                            #     #
                            ##   ##   ##   # #    #
                            # # # #  #  #  # ##   #
                            #  #  # #    # # # #  #
                            #     # ###### # #  # #
                            #     # #    # # #   ##
                            #     # #    # # #    #
################################################################################

function main(filename=filename, mintad=mintad, maxsize=maxsize, batchsize=batchsize, limit=limit, good_cache=good_cache, bad_cache=bad_cache, done_cache=done_cache, time_cache=time_cache)

    good_pop = load_good_pop(filename)

    # make sure there is something to start with
    if length(good_pop) == 0
        println("$progname: ERROR: There were no valid good solutions in '$filename' to start with.")
        exit()
    end

    # prepare stepping
    s = good_pop
    print_status(s)

    # make sure that we do not have too many matrices
    fix = false
    if length(s) > maxsize
        println("Max size hit. Attempting to fix.")
        # find out if we can cut it
        maxtad, total = get_maxtad_and_total(s, maxsize)
        if total > maxsize
            # if it would be too big after a cut, prompt for new maxsize
            maxsize = prompt_maxsize(total, maxsize)
            maxtad, total = get_maxtad_and_total(s, maxsize)
            fix = true
        end
        # make sure that the cut does not remove matrices we want to keep
        if maxtad < mintad
            mintad = prompt_mintad(maxtad, mintad)
        end
        # now we can remove
        s = remove_large(s, maxtad)
        fix = !fix
    end

    # if we had to fix the number of matrices, print the status again
    fix && print_status(s)

    # Take care of the save-file
    if isfile(filename * "_spider-remain")
        println("Warning: Save file '$(filename * "_spider-remain")' exists. Moving to '$(filename * "_spider-remain_backup")' (even if that exists.)")
        mv(filename * "_spider-remain", filename * "_spider-remain_backup", force=true)
    end
    if isfile(filename * "_spider-processed")
        println("Warning: Save file '$(filename * "_spider-processed")' exists. Moving to '$(filename * "_spider-processed_backup")' (even if that exists.)")
        mv(filename * "_spider-processed", filename * "_spider-processed_backup", force=true)
    end

    # Save the starting matrices
    open(filename * "_spider-remain", "w") do f
        serialize(f, s)
    end

    for m in s
        time_cache[hash(m)] = 0
    end

    # start stepping
    fs = s
    iterations = 0
    while true
        # create the batch to step
        # get the maximum tadpole we can sample from
        bmaxtad, btotal = get_maxtad_and_total(fs, batchsize)
        # remove too large tadpoles
        bs = remove_large(fs, bmaxtad)

        oldest = max([time_cache[hash(m)] for m in bs]...)
        age = 1
        # if there are too many, we first try to remove the young ones
        while btotal > batchsize && age ≤ oldest
            bs = [ m for m in bs if time_cache[hash(m)] ≥ age ]
            btotal = length(bs)
            age += 1
        end

        # make a sample
        bs = sample(bs, min(batchsize,length(bs)), replace=false)

        # every now and then, take a true random sample to try and escape a
        # potential minima
        iterations += 1
        for k in keys(time_cache)
            time_cache[k] = time_cache[k] + 1
        end
        if iterations % 10 == 0
            bs = sample(fs, min(batchsize,length(fs)), replace=false)
        end

        # show it
        print_status(bs,:proc)
        # and step it
        ss = step(bs, limit, Inf, good_cache, bad_cache, done_cache)
        for m in ss
            if hash(m) ∉ keys(time_cache)
                time_cache[hash(m)] = 0
            end
        end
        # print new ones
        print_status(setdiff(ss,s),:found)
        # update s
        s = union(s,ss)

        # print result
        print_status(s)

        # save
        fs = setdiff(s, filter_calculated(s,done_cache))
        open(filename * "_spider-remain", "w") do f
            serialize(f, fs)
        end
        open(filename * "_spider-processed", "w") do f
            serialize(f, setdiff(s, fs))
        end

        # make sure the saved set is smaller than maxsize
        fix = false
        if length(s) > maxsize
            println("Max size hit. Attempting to fix.")
            # find out if we can cut it
            maxtad, total = get_maxtad_and_total(s, maxsize)

            if total > maxsize
                # if it would be too big after a cut, prompt for new maxsize
                maxsize = prompt_maxsize(total, maxsize)
                maxtad, total = get_maxtad_and_total(s, maxsize)
            end

            # make sure that the cut does not remove matrices we want to keep
            if maxtad < mintad
                mintad = prompt_mintad(maxtad, mintad)
            end
            ns = remove_large(s, maxtad)
            for h in hash.(setdiff(s,ns))
                delete!(time_cache, h)
                delete!(done_cache, h)
            end
            s = ns
            fix = !fix
        end
        # compute the new filtered set for the next step
        fix && (fs = setdiff(s, filter_calculated(s,done_cache)))
        # check if there is something left to do
        if length(fs) == 0
            println("$progname: Info: Nothing left to do. Will exit.")
            exit()
        end
        # if there is still work to be done...

        # ... and if we had to fix the number of matrices, print the status again
        fix && print_status(s)

        # ... and save
        fix && open(filename * "_spider-remain", "w") do f
            serialize(f, fs)
        end
        fix && open(filename * "_spider-processed", "w") do f
            serialize(f, setdiff(s, fs))
        end

        print_status(fs,:filter)

        good_cache = Dict{UInt64,Float64}()
        bad_cache = Dict{UInt64,Float64}()

        GC.gc()
    end

end

#     #                 #
#     #mmm    mmm    mmm#  m   m
#     #" "#  #" "#  #" "#  "m m"
#     #   #  #   #  #   #   #m#
#     ##m#"  "#m#"  "#m##   "#
#                           m"
#                          ""


# load solver with e8roots-file and load utils

dir = String(@__DIR__)

solverfile = dir * "/../" * solver
include(solverfile)

utilfile = dir * "/minitad_utils/spider_utils.jl"
include(utilfile)

# fitness

nfit(x) = calc_fit(x, verbose=false, save=false, detailed=true)

# start cache

good_cache = Dict{UInt64,Float64}()
bad_cache = Dict{UInt64,Float64}()
done_cache = Dict{UInt64,Float64}()
time_cache = Dict{UInt64,Int64}()

# run main

main(filename, mintad, maxsize, batchsize, limit, good_cache, bad_cache, done_cache, time_cache)
