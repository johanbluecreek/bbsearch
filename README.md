# bbsearch

[[_TOC_]]

## Introduction

I have found [BlackBoxOptim.jl](https://github.com/robertfeldt/BlackBoxOptim.jl) (BBO) to be a very useful tool for solving problems I have encountered. The kind of problems we aim to solve here are global optimisation problems where you do not necessarily have a gradient to guide your search.

There are many potential ways one can use BBO, such as writing your own Julia scripts for solving the problems or working in a [Jupyter](https://jupyter.org/) notebook. If you are not too careful, however, you will eventually run into the problem where you do not remember what your fitness function was for a certain run, or where you stored the result of a run, and so on.

This is where bbsearch is suppose to help. It aims to provide the following:

  * Implementing your own problem with low effort
  * Start new and continue on previous runs effortlessly
  * Save all runs
  * Log run inputs and results
  * Protection from starting a run on code that cannot be traced back

For beginners there is a "Beginner's guide" below that will guide you through how to install Julia, git, and finally bbsearch. If you are more experienced, consider the sections "Basic usage" and "Suggested work-flow", which should put you to work in a few moments.

If any of these instructions are unclear you can contact me preferably by starting an issue here on gitlab under "Issues" on the bar to the left. If you do not have, or do not want an account, you can send me an e-mail.

## Beginner's guide

If you just heard about bbsearch frame-work and want to try it out, then you should probably follow these instructions first. These will include links to instruction on how to install Julia, and the relevant Julia packages, and how to execute a run. It will also show where to find instructions for installing git, which is important for several features of bbsearch.

### Installing git

#### Motivation and why bbsearch uses git

Git is a so-called "version control system", which means that it is a program that handles all the changes you make. To illustrate what this means, lets consider an example. You are working on a project, and you might realise that you now have deleted or rewritten several lines, but you want to go back. In git you make "commits", which are basically save-points which you can go back to at any time. So in the hypothetical, if you have a commit where your original text was, and a new commit where your erroneous edits were made, you can go back to the original.

Git will be used here in several ways. First it is how you get bbsearch. You will "clone" this project to your own computer. Second, the more important, is that git will be used by bbsearch to makes sure you do not lose work.

As you should already know, bbsearch will try to minimise a problem. If it does not succeed on the first try, you may have to modify some aspect of the problem formulation, and then perform another search. By making a commit of your changes each time, bbsearch will be able to log the result of each run and associate that result with a specific commit. So if you forgot what yielded an old result, you can always go back to it.

We will describe the basic usage necessary under the "Suggested work-flow" section later.

#### Install instructions

The install instructions are found here:

[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

find the instructions for your operating system, and follow them.

For many systems, linux and MacOS, git may already be present. Open a terminal and run
```bash
$ git --version
git version 2.20.1
```
(to be clear: the `$` just means that it is a terminal prompt, and it should not be typed out) and see if a verison number is returned, like in the example above.

#### Working with git

General work with git it too long of a subject to cover here. Throughout this readme, some basic functionalities will be covered, just to make bbsearch work as intended. For any advanced usage, there are many tutorials and answered questions online that can help you.

### Installing Julia

This topic is covered well else where, so please consult:

  * Downloads: [https://julialang.org/downloads/](https://julialang.org/downloads/)
  * Install instructions: [https://julialang.org/downloads/platform.html/](https://julialang.org/downloads/platform.html/)

For linux and brew (MacOS): note that Julia may already exist in your package manager so it may be good to look there first. Note that you must have a version ≥ 1.0.

Once installed you should be able to start Julia by typing
```bash
$ julia
```
in a terminal. This will give you a Julia prompt. You can exit Julia by `Ctrl+d` or typing `exit()`.

### Installing packages

To be able to run bbsearch, we need to install some packages this frame-work depends on. This is how you install the dependencies for bbsearch:

```
$ julia
julia> using Pkg
julia> Pkg.add("ArgParse")
julia> Pkg.add("Serialization")
julia> Pkg.add("BlackBoxOptim")
```

That is, first we start Julia, then we get access to Julia's package management (`Pkg`) and then we install the packages `ArgParse`, `Serialization`, and `BlackBoxOptim`.

#### Other packages (optional)

Some problems needs other packages to be formulated, and they are loaded on a by-need basis, and defined in the problem files, that is under `solvers/`. Consult all `using` lines in the problem file you want to load and install those packages as well.

The following command should give you the packages needed for the the problem `demo`
```bash
$ grep using solvers/minitad_fair.jl | awk '{print $2}'
```

So if you want to run a predefined problem, modify the above and install the packages displayed, if any.

### Installing bbsearch

Installing bbsearch can be done in several ways. I would recommend that you clone this repository into a folder of your choice. This is done for example as so:
```bash
$ cd prefered/folder/
$ git clone https://gitlab.com/johanbluecreek/bbsearch.git
```
you can now see a new folder `bbsearch/` being created, and from now on it will be assumed that you are standing in this folder;
```bash
$ cd bbsearch/
```

### Running a demonstration

You should now be able to make your first run. bbsearch provides several options to modify your search. These options can be see by typing
```bash
$ ./bbsearch.jl --help
```
It will take a while to execute every command as Julia needs to compile at every execution. Just be patient, and think carefully about what you want to run.

To just try a generic run, we can execute the following:
```bash
$ ./bbsearch.jl -n -s demo -r 2*10.0 -c 10.0 -p 100
```
This will start a new run (`-n`) of the demonstration problem called `demo` (`-s demo`). The run will go on for 20 seconds (`-r 2*10.0`) and will save the solution at 10 second intervals (`-c 10.0`). The run will use a population of 100 candidate solutions (`-p 100`).

## Basic usage

### Help message

bbsearch comes with a help message that displays all available options:

```
$ ./bbsearch.jl --help
usage: bbsearch.jl [-s SOLVER] [--listsolvers] [-n] [-l LOADFILE]
                   [-f PRINTFILE] [-r RUNTIME] [-c CUTTIME]
                   [-p POPSIZE] [-d DISTANCE] [--savefile SAVEFILE]
                   [--savepath SAVEPATH] [-q] [-m METHOD]
                   [--listmethods] [--logfile LOGFILE] [-h]

optional arguments:
  -s, --solver SOLVER   select solver to run (default: "demo")
  --listsolvers         list available solvers
  -n, --new             start a new run
  -l, --loadfile LOADFILE
                        give filename of file to load (this toggles a
                        run of the loaded file) (default: "")
  -f, --printfile PRINTFILE
                        prints the result saved in file, and exits
                        (default: "")
  -r, --runtime RUNTIME
                        runtime for search in seconds (unevaluated
                        expressions allowed) (default: "120.0")
  -c, --cuttime CUTTIME
                        cuts the runtime in the given amount of
                        seconds to save the result at those points
                        (unevaluated expressions allowed) (default:
                        "")
  -p, --popsize POPSIZE
                        size of population for search (type: Int64,
                        default: 50)
  -d, --distance DISTANCE
                        set maximum value for the parameter distance
                        (final range: (-[dist],[dist])) (type:
                        Float64, default: 1.0)
  --savefile SAVEFILE   give filename of savefile (relative
                        `savepath`) (default:
                        "save_20191106-16:57:04")
  --savepath SAVEPATH   give an alternate path for saves (default:
                        "saves")
  -q, --quiet           Keeps some messages suppressed
  -m, --method METHOD   selects method to use for optimisation
                        (default:
                        "adaptive_de_rand_1_bin_radiuslimited")
  --listmethods         lists available methods for optimisation
  --logfile LOGFILE     set a path and name to a custom log file
                        (default: "search.log")
  -h, --help            show this help message and exit
```

To illustrate some of these, we will not turn to some examples.

### Examples

Starting a new run is done by the `-n` flag, so the if you want to solve the problem `demo` you specify this with `-s`
```bash
$ ./bbsearch.jl -n -s demo
```
This will select defaults for all other parameters, which also are mostly the defaults also for BBO.

To load a previous run this can be done by `-l`. To not accidentally create unexpected behaviours, a loaded run will only work if the `-s` option is set to the same name. So say the above run finished and saved the run as `save_20190930-17:04:38_1`, then it can be loaded by
```bash
$ ./bbsearch.jl -l saves/save_20190930-17:04:38_1 -s demo
```

By default saves are stored under `saves/` and are given names according to date and time. You can change either of these independently
```bash
$ ./bbsearch.jl -l saves/save_20190930-17:04:38_1 --savepath new_saves --savefile testsave -s demo
```
will save the result in `new_saves/` as `testsave`.

You can also provide a custom log file
```bash
$ ./bbsearch.jl -n --logfile logs/new_search.log -s demo
```



## Suggested work-flow

So far we have gone through how to start and resume a run, and also some options. This is not why you are here, but to be able to solve your own problems. This section contains instructions on how to design your own problem and start developing.

### Designing your problem

Under the subfolder `solvers/` your will find some files containing pre-defined problems. Here you want to create your own file containing your own problem.

The following is optional, but recommended: Start with creating a git-branch on which you want to develop your problem and start working
```bash
$ git checkout -b branch_name
```
where you replace `branch_name` with preferred name.

Now you can create a file, `problem_name.jl`, under `solvers/`, we will be using the place-holder name `problem_name` here. This file is expected to contain at least a single function: `calc_fit()` which calculates the fitness of your problem -- the number that should be optimised (minimised). You can take a look at the other files in `solvers/` to see how this can be implemented.

Creating the function `calc_fit()` is (more-or-less) all you need to do programming-wise to start solving your problem. These are the details you need to know:

  * The first argument given to this will be a one-dimensional array of floats (`Float64[]`).
  * Any other arguments must be optional.
  * It must return a `Float64` number.

Optionally, you can add a `verbose` keyword option to this function to trigger when the function should print additional information about the solution that is given to it. This will be printed at the end of a run, or in between saves.

The last thing you need to do is to define the search space you want to restrict the search to. This is done by adding a line defining `parsed_args["searchspace"]` to your `problem_name.jl` file. Search spaces are something the BBO handles, and you can generally use
```julia
parsed_args["searchspace"] = RectSearchSpace(y,dimdigits = z)
```
where `y` is a vector of ranges and `z` is a vector containing the number of digits after the decimal point that should be present. This means, for example, if you have a problem taking an input vector of two entries, the first being integer between ±100 and the second continuous between (0.0,1.0), then the search space can be given as
```julia
parsed_args["searchspace"] = RectSearchSpace([(-100.0,100.0), (0.0,1.0)],dimdigits = [0, -1])
```

Since you may want to change these ranges on the fly, and bbsearch prevents you to do this without making a new git commit, there is the `-d` option that can be added, which functions as an optional scale. That is, say in the above example you are interested in testing various ranges between ±100 and ±1000, then you can give the search space as
```julia
parsed_args["searchspace"] = RectSearchSpace([(-100.0*parsed_args["distance"],100.0*parsed_args["distance"]), (0.0,1.0)],dimdigits = [0, -1])
```
then starting bbsearch with `-d 1.0` (default) searches ±100 and `-d 10.0` searches ±1000.


### Making your own problem runnable

To be able to run and solve `problem_name` problem, you need to let bbsearch know about this problem. At this point in time you have to add `problem_name` in one places in the code of `bbsearch.jl`: There is a function `get_problemlist()` that returns a list, add `"problem_name"` to that list (must be equal to the filename, excluding the file-ending).

This is a one-time edit that you do not have to come back to unless you want to create a new problem to solve.

### Running your problem

Following the previous steps, you would still not be able to run your problem. This is because bbsearch will check if your problem-file `problem_name.jl` and `bbsearch.jl` both have all changes committed to the git repository. This is a protection for you: you cannot make a run on files with changes that you may later lose. bbsearch will print a warning for you and exit if it detects any such discrepancy.

If you do not know how you make commits, this is the most simple way
```bash
$ git add solvers/problem_name.jl
$ git commit -a -m "[ENTER MESSAGE HERE]"
```
The first line is only necessary the first time you make a commit for a new problem. The second line can be used only after that. The message is there to give you a hint on what changes you did. If you look at the output of:
```bash
$ git log
```
you will see the messages you have typed.

Now we can run the problem:
```bash
$ ./bbsearch -n -s problem_name
```
The above command will run with default options, which are similar the defaults of BBO. That is it will be a run for 120 seconds, with a population of 50, and using the "adaptive DE/rand/1/bin radius-limited" algorithm.

### When a run is finished

When a run has completed, you will see that by being returned to your shell-prompt (the `$`) and the `verbose` message of your `calc_fit()` function will be displayed.

A finished run will be saved as a new file under the subfolder `saves/` in this repository, per default. This save contains: the optimisation problem that was run (`opt`), the optimisation result (`res`), and the options you ran bbsearch with (`parsed_args_old`) and you can load a file in Julia by e.g.:

```julia
using Serialization
filename = "name_of_savefile"
opt, res, parsed_args_old = open(pwd() * "/saves/" * filename, "r") do sf
      deserialize(sf)
end
```

This run will also be logged in the `search.log` file. There are some important entries to consider there. Take the following example log entry:

```
---
20191106-16:42:08 :  New run (00f02713981b31c7a8e61740650484749571831a) with arguments:
20191106-16:42:08 :  savepath => saves
20191106-16:42:08 :  printfile =>
20191106-16:42:08 :  method => adaptive_de_rand_1_bin_radiuslimited
20191106-16:42:08 :  popsize => 50
20191106-16:42:08 :  savefile => save_20191106-16:41:59
20191106-16:42:08 :  distance => 5000.0
20191106-16:42:08 :  cuttime => 10.0
20191106-16:42:08 :  quiet => false
20191106-16:42:08 :  solver => demo
20191106-16:42:08 :  listmethods => false
20191106-16:42:08 :  savefile_actual => saves/save_20191106-16:41:59_1
20191106-16:42:08 :  logfile => search.log
20191106-16:42:08 :  searchspace => ContinuousRectSearchSpace([...], [...], [...])
20191106-16:42:08 :  loadfile =>
20191106-16:42:08 :  hash => 00f02713981b31c7a8e61740650484749571831a
20191106-16:42:08 :  new => true
20191106-16:42:08 :  runtime => 10.0
20191106-16:42:08 :  listsolvers => false
20191106-16:42:08 :  cuts => 1
20191106-16:42:08 :  and result:
20191106-16:42:08 : Fitness: 6.162975822039155e-32
```

Each entry is separated by a `---` and all lines are indexed by the date and time the entry was made. The first line will we of three types: "New", "Continued", or "Loaded". "New" simply means a new run was started (or `new => true`), "Continued" means that the run was started earlier, and this is just an intermediate, or final, save of the run (can also be seen as the `savefile_actual` entry ending in a number higher than 1). Lastly "Loaded" means that the file was loaded from a previously saved file (if the `loadfile` entry is non-empty).

To load the file that the above log entry is about, we would have to use the file `save_20191106-16:41:59_1`, found under the `savefile_actual` entry. Do not use tab-completion or any escape-characters when entering the filename (if you do not know what this means, it can be ignored), and the file would be loaded and continued on with
```bash
$ ./bbsearch.jl -l saves/save_20191106-16:41:59_1
```

On the first line, and under the `hash` entry, you can see which git commit this run belongs to. This so that if you ever need to return to this particular run, you can ask git to show you the full code that this run was made on. You can look at the git-log to get all the commits with their messages, dates, and hashes.

The last entry is also very important, as it shows the output of the `calc_fit()` function at the time the log entry was made.

This way, using the save files, the log, and git you are protected from losing any work and can always go back. Note that your save files are not included in the git-repository, as they may be large binary files, **so you have to keep them safe by yourself**.

If you ever want to go back and check a solution's verbose message again, you find the entry in the log, and using the `savefile_actual`-name you can run:

```bash
$ ./bbsearch.jl -f <savefile_actual-name>
```
and that solution's best-candidates verbose message will be reprinted. This can be important to do, as you edit your `problem_name.jl` the fitness values may not be the same any more (that is the value found in `search.log`), so you may want to run the above just to see if it has changed.

### Fine-tuning your runs

This is where most of the work will be done. If the first run with all default values do not give a good result, you may wish to alter your `calc_fit()` or alter the parameters for the search.

To change the parameters that defined your search you can consult the help option for bbsearch. As an example, the following command:

```bash
$ ./bbsearch.jl -n -s problem_name -c 30*60.0 -r 4*60*60.0 -p 150 -d 7.0 -m separable_nes --savefile testruns
```

will start a new run (`-n`) using the `calc_fit()` defined for `problem_name` (`-s problem name`). Every 30 minutes the search will stop, print the progress, add a log entry, save a file, and then continue (`-c 30*60.0`) for a total of four hours (`-r 4*60*60.0`). The search will be performed using a population of 150 individuals (`-p 150`) and the input array given to `calc_fit()` will have entries in the span [-7.0,7.0] (`-d 7.0`) (see also TODO items 2.~and 3.). The base-name for the save-file for this run will be `testruns` (`--savefile testruns`), and hence the save files will be called `testruns_1`, `testruns_2`, etc. for each cut.

Note that some of these parameters only matter for a new run, and cannot be changed on a loaded run. For example population size is fixed the first time a run is started, while run times and cut times can be changed when the run is loaded.

### Summary

Your work-flow should look somewhat like:

  - Setup:
    * Create a git-branch and check it out
    * Create a `calc_fit()` in a problem file
    * Add problem name to `bbsearch.jl`
    * Commit changes
  - Repeat:
    * Modify `calc_fit()` as needed
    * Commit changes
    * Modify command-line arguments/search parameters as needed
    * Start a run

until a satisfactory solution has been found.

## Included problems

With bbsearch there are some problems already defined. These are here to take inspiration from in developing new problems and can be used on the fly. In this section we go through some details of these problems.

### demo.jl

The `demo.jl` problem minimizes a twelve dimensional "Rosenbrock" function. This
solver or problem file illustrates the following points:

  * How to enter a search space where different directions of the search space have different sizes
  * How to implement the scale that can be set by `-d`
  * How to write a simple `calc_fit()` function
  * How to include a `verbose` message, which bbsearch displays when finishing or loading a run.

The problem is very easy to solve. By scaling up the search space you can make it take more than an instance
```bash
./bbsearch.jl -n -s demo -r 10.0 -d 5000
```

### minitad (minitad_common.jl)

The `minitad` solvers and the utility tools included are part of the paper [arXiv:2010.10519](https://arxiv.org/abs/2010.10519), and also used for the paper [arXiv:2103.03250](https://arxiv.org/abs/2103.03250).

The `minitad_common.jl` is the main solver with the whole fitness function, and modified version of that fitness function can be made by importing the `minitad_common.jl` and overwriting the defaults. To demonstrate this we have included `minitad_e8uuu.jl` which is a solver for a smaller problem.

The problem being solved is completely documented in [arXiv:2010.10519](https://arxiv.org/abs/2010.10519). To summarize the problem being solved: Find a matrix `N`, such that the matrix `M = NdNᵀd` (and `M' = NᵀdNd`), `d` being 'the intersection matrix', will

  * `M` has real positive semi-definite eigenvalues
  * `M` and `M'` are diagonalizable
  * eigenvectors of the same signed norms if there are non-distinct eigenvalues
  * have no vector with norm-squared equal to 2 which is orthogonal to all `p` eigenvectors (for `M` and `M'`) of positive norm-square.
  * as small tadpole, `½ Tr M`, as possible

The goal for this solver is to attempt and establish a minimum tadpole possible.

For the utility tools, see also the `README.md` in `solvers/minitad_utils/` where those are documented.

## TODO

  * Perhaps find a way of parsing args using python instead. Julia is so slow, so for checking if you run from a clean git takes a lot of time.
  * Write a series of scripts or so for parsing the `search.log`. Things that would be handy:
    - Parse out one search tree completely (from new, continued, and different loads)
    - Parse out by problem being solved
    - Find best fitness found for problem
  * Make git options:
    - `--lazy-git`  that commits and prompts the user for a message
