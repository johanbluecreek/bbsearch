#!/usr/bin/env julia

####################################################
# bbsearch[.jl] is licensed under GPL-3.0-or-later #
# Copyright (c) 2019: Johan Blåbäck                #
# See file `COPYING` for details.                  #
####################################################

using ArgParse
using Serialization
using BlackBoxOptim

const progname = "bbsearch"

dir = String(@__DIR__)
const gitdir = dir * ""
const solverdir = dir * "/solvers"

# Exit codes
# 0 - exited as intended.
# 1 - file-related errors
# 2 - input argument related errors
# 3 - git errors
# 4 - solver problem

################################################################################
           ###### #    # #    #  ####  ##### #  ####  #    #  ####
           #      #    # ##   # #    #   #   # #    # ##   # #
           #####  #    # # #  # #        #   # #    # # #  #  ####
           #      #    # #  # # #        #   # #    # #  # #      #
           #      #    # #   ## #    #   #   # #    # #   ## #    #
           #       ####  #    #  ####    #   #  ####  #    #  ####
################################################################################

"""
    buildfun(name, expr_file, vars)

Function that builds a function named `name`, that evaluates the expression from
the file `expr_file` dependent on `vars`.
"""
function buildfun(name::String, expr_file::String, vars::String)
    expr = open(expr_file) do file
        read(file, String)
    end

    fun_string = """
    begin
        function $name($vars)
            $expr
        end
        $name(x::Array{Float64,1}) = $name(x...)
        $name(x::Vector) = $name(x...)
    end
    """

    eval(Meta.parse(fun_string))
end

################################################################################
           ##   #####   ####  #    # #    # ###### #    # #####  ####
          #  #  #    # #    # #    # ##  ## #      ##   #   #   #
         #    # #    # #      #    # # ## # #####  # #  #   #    ####
         ###### #####  #  ### #    # #    # #      #  # #   #        #
         #    # #   #  #    # #    # #    # #      #   ##   #   #    #
         #    # #    #  ####   ####  #    # ###### #    #   #    ####
################################################################################

"""
    parse_commandline()

Parses arguments given on the commandline.
"""
function parse_commandline()

    default_values = Dict([
        "solver" => "demo",
        "listsolvers" => false,
        "new" => false,
        "loadfile" => "",
        "printfile" => "",
        "distance" => 1.0,
        "runtime" => "120.0",
        "cuttime" => "",
        "popsize" => 50,
        "savefile" => "save_" * Libc.strftime("%Y%m%d-%H:%M:%S",time()),
        "savepath" => "saves",
        "quiet" => false,
        "method" => "adaptive_de_rand_1_bin_radiuslimited",
        "listmethods" => false,
        "logfile" => "search.log",
        "ignoregit" => false,
        "keep" => 0
    ])

    s = ArgParseSettings()

    @add_arg_table! s begin
        "--solver", "-s"
            help = "select solver to run"
            default = default_values["solver"]
        #
        "--listsolvers"
            help = "list available solvers"
            action = :store_true
        #
        "--new", "-n"
            help = "start a new run"
            action = :store_true
        #
        "--loadfile", "-l"
            help = "give filename of file to load (this toggles a run of the loaded file)"
            default = default_values["loadfile"]
        #
        "--printfile", "-f"
            help = "prints the result saved in file, and exits"
            default = default_values["printfile"]
        #
        "--runtime", "-r"
            help = "runtime for search in seconds (unevaluated expressions allowed)"
            arg_type = String
            default = default_values["runtime"]
        #
        "--cuttime", "-c"
            help = "cuts the runtime in the given amount of seconds to save the result at those points (unevaluated expressions allowed)"
            arg_type = String
            default = default_values["cuttime"]
        #
        "--popsize", "-p"
            help = "size of population for search"
            arg_type = Int64
            default = default_values["popsize"]
        #
        "--distance", "-d"
            help = "set maximum value for the parameter distance (final range: (-[dist],[dist]))"
            arg_type = Float64
            default = default_values["distance"]
        #
        "--savefile"
            help = "give filename of savefile (relative `savepath`)"
            default = default_values["savefile"]
        #
        "--savepath"
            help = "give an alternate path for saves"
            default = default_values["savepath"]
        #
        "--quiet", "-q"
            help = "Keeps some messages suppressed"
            action = :store_true
        #
        "--method", "-m"
            help = "selects method to use for optimisation"
            default = default_values["method"]
        #
        "--listmethods"
            help = "lists available methods for optimisation"
            action = :store_true
        #
        "--logfile"
            help = "set a path and name to a custom log file"
            default = default_values["logfile"]
        #
        "--ignoregit"
            help = "ignore verifying that all modifications are commited into the git repo"
            action = :store_true
        #
        "--keep"
            help = "numbers of cuts to save (0 is all)"
            arg_type = Int64
            default = default_values["keep"]
    end

    return parse_args(s)
end

function get_arguments(gitdir=gitdir)

    parsed_args = parse_commandline()

    # Parse out incompatible options
    if parsed_args["new"] && !(parsed_args["loadfile"] == "")
        println("$progname: new (-n/--new) and load (-l/--loadfile) are incompatible.")
        exit(2)
    elseif parsed_args["new"] && !(parsed_args["printfile"] == "")
        println("$progname: new (-n/--new) and print (-f/--printfile) are incompatible.")
        exit(2)
    elseif !(parsed_args["loadfile"] == "") && !(parsed_args["printfile"] == "")
        println("$progname: load (-l/--loadfile) and print (-f/--printfile) are incompatible.")
        exit(2)
    end

    # If listing solvers, exit early
    if parsed_args["listsolvers"]
        println("The available solvers are:")
        for l in get_solverlist()
            println(l)
        end
        exit()
    end

    # If listing methods, exit early
    if parsed_args["listmethods"]
        println("The available methods are:")
        for l in get_methodlist()
            println(l)
        end
        exit()
    end

    # Try and get the git-hash of the repo to be able to trace where the reult is coming from
    try
        parsed_args["hash"] = read(`bash -c "cd $gitdir && git rev-parse HEAD"`, String)[1:end-1]
    catch
        parsed_args["hash"] = "no_git_available: " * Libc.strftime("%Y%m%d-%H:%M:%S",time())
    end

    # Parse runtime and cuttime into numbers
    parsed_args["runtime"] = eval(Meta.parse(parsed_args["runtime"]))
    if parsed_args["cuttime"] == ""
        parsed_args["cuttime"] = parsed_args["runtime"]
    else
        parsed_args["cuttime"] = eval(Meta.parse(parsed_args["cuttime"]))
    end

    parsed_args["cuts"] = length(0.0:parsed_args["cuttime"]:parsed_args["runtime"]) - 1

    return parsed_args
end

################################################################################
                ####   ####  #      #    # ###### #####   ####
               #      #    # #      #    # #      #    # #
                ####  #    # #      #    # #####  #    #  ####
                    # #    # #      #    # #      #####       #
               #    # #    # #       #  #  #      #   #  #    #
                ####   ####  ######   ##   ###### #    #  ####
################################################################################

function get_solverlist(solverdir=solverdir)
    return replace.(filter(x -> occursin(".jl", x), readdir(solverdir)), ".jl" => "")
end

function get_solver_filename(parsed_args)
    solver_list = get_solverlist()
    if parsed_args["solver"] in solver_list
        filename = joinpath("solvers", parsed_args["solver"] * ".jl")
        return filename
    else
        println("$progname: No solver by the name '$(parsed_args["solver"])' found.")
        println("Available solvers are:")
        for s in solver_list
            println(s)
        end
        exit(2)
    end
end

function print_solverlist()
    solver_list = get_solverlist()
    for s in solver_list
        println(s)
    end
end
################################################################################
                 ####   ####  #    # ##### #####   ####  #
                #    # #    # ##   #   #   #    # #    # #
                #      #    # # #  #   #   #    # #    # #
                #      #    # #  # #   #   #####  #    # #
                #    # #    # #   ##   #   #   #  #    # #
                 ####   ####  #    #   #   #    #  ####  ######
################################################################################

#=
Here we have some control-related functions for handling programming stuff, not
directly search related stuff.
=#

"""
    get_methodlist()

Returns list of method names.
"""
function get_methodlist()
    return [
        "dxnes",
        "adaptive_de_rand_1_bin_radiuslimited", # BlackBoxOptim.jl's default
        "xnes",
        "de_rand_1_bin_radiuslimited",
        "adaptive_de_rand_1_bin",
        "generating_set_search",
        "de_rand_1_bin",
        "separable_nes",
        "resampling_inheritance_memetic_search",
        "probabilistic_descent",
        "resampling_memetic_search",
        "de_rand_2_bin_radiuslimited",
        "de_rand_2_bin",
        "random_search",
        "simultaneous_perturbation_stochastic_approximation"
    ]
end

"""
    print_methodlist()

Prints list of availalbe methods
"""
function print_methodlist()
    ms = get_methodlist()
    for m in ms
        println(m)
    end
end

function save_run(opt, res, parsed_args)
    filename = parsed_args["savefile_actual"]

    if !isdir(parsed_args["savepath"])
        println("$progname: Save directory not found. Creating directory.")
        dir = mkpath(parsed_args["savepath"])
        println("$progname: $dir")
    end

    open(filename, "w") do sf
        serialize(sf, (opt, res, parsed_args))
    end
end

"""
    load_file(parsed_args)

Loads search results and optimisation problem from saved file.
"""
function load_file(parsed_args)

    # Figure out the reason we are loading a file:
    filename = ""
    if !(parsed_args["loadfile"] == "")
        filename = parsed_args["loadfile"]
    else
        filename = parsed_args["printfile"]
    end
    #
    files = readdir(dirname(filename))

    # If a faulty filename has been given to the command line, prompt and exit
    if !(basename(filename) in files)
        println("$progname: File not found: $filename")
        exit(1)
    end

    opt, res, parsed_args_old = open(filename, "r") do sf
        deserialize(sf)
    end

    return opt, res, parsed_args_old
end

function log_run(res, parsed_args, option)

    log_hash = parsed_args["hash"]

    if option == :new
        log_head = "New run ($log_hash) with arguments:"
    elseif option == :load
        log_head = "Loaded run ($log_hash) with arguments:"
    elseif option == :cont
        log_head = "Continued run ($log_hash) with arguments:"
    end

    log_time = Libc.strftime("%Y%m%d-%H:%M:%S",time())
    log_args = *(["$log_time :  $arg => $val\n" for (arg, val) in parsed_args if arg ≠ "searchspace"]...)
    log_args = log_args[1:end-1]

    best = best_candidate(res)

    log_res = "$log_time : Fitness: $(calc_fit(best))"

    log_message =   """
                    ---
                    $log_time :  $log_head
                    $log_args
                    $log_time :  and result:
                    $log_res
                    """

    open(parsed_args["logfile"], "a") do f
        write(f, log_message)
    end
end

function print_res(res)
    calc_fit(best_candidate(res); verbose=true)
end

"""
    print_file(parsed_args)

Prints result from saved file.
"""
function print_file(parsed_args)
    opt, res, parsed_args_old = load_file(parsed_args)
    # Verify that the same solver is loaded
    if !(parsed_args_old["solver"] == parsed_args["solver"])
        println("$progname: ERROR: The solver has changed. Will now exit.")
        exit(4)
    end
    print_res(res)
end

"""
    print_save(parsed_args)

Prints name of save_file
"""
function print_save(parsed_args)
    println("Last save: $(parsed_args["savefile_actual"])")
end

"""
    remove_cuts(parsed_args, cut)

Removes cuts except the last parsed_args["keep"].
"""
function remove_cuts(parsed_args, cut)
    if cut > parsed_args["keep"] && parsed_args["keep"] > 0
        rm(joinpath(parsed_args["savepath"], parsed_args["savefile"] * "_" * string(cut-(parsed_args["keep"]))))
    end
end

################################################################################
                   ####   ####  #      #    # # #    #  ####
                  #      #    # #      #    # # ##   # #    #
                   ####  #    # #      #    # # # #  # #
                       # #    # #      #    # # #  # # #  ###
                  #    # #    # #       #  #  # #   ## #    #
                   ####   ####  ######   ##   # #    #  ####
################################################################################

"""
    setup_problem(parsed_args)

Returns an optimisation-problem from bbsetup()
"""
function setup_problem(parsed_args, pop=nothing)

    ms = get_methodlist()

    if !(parsed_args["method"] in ms)
        println("$progname: ERROR: Selected method not available.")
        println("You selected method: $(parsed_args["method"])")
        print_methodlist()
        exit(2)
    end

    method = Meta.parse(parsed_args["method"])

    if pop == nothing
        opt = bbsetup(
            calc_fit;
            SearchSpace = parsed_args["searchspace"],
            PopulationSize = parsed_args["popsize"],
            Method = method
        )
    else
        opt = bbsetup(
            calc_fit;
            SearchSpace = parsed_args["searchspace"],
            PopulationSize = parsed_args["popsize"],
            Method = method,
            Population = pop
        )
    end

    return opt
end

"""
    start_run(opt)

Passes an optimisation-problem to bboptimize()
"""
function start_run(opt, parsed_args)
    res = bboptimize(
        opt;
        MaxTime = parsed_args["cuttime"]
    )

    return opt, res
end

################################################################################
                            #    #   ##   # #    #
                            ##  ##  #  #  # ##   #
                            # ## # #    # # # #  #
                            #    # ###### # #  # #
                            #    # #    # # #   ##
                            #    # #    # # #    #
################################################################################

function main(parsed_args, gitdir=gitdir)

    # Check that git has been committed
    # This is so that we do not make a run on something uncommitted, which
    # makes it harder to figure out where a run came from, since they are
    # indexed by git hashes
    if !parsed_args["ignoregit"]
        git_dirty = success(run(`bash -c "cd $gitdir && git status | grep $progname.jl"`, wait=false))
        if git_dirty
            println("$progname: The file $progname.jl is not commited to git.")
        end
        git_dirt = success(run(`bash -c "cd $gitdir && git status | grep $(parsed_args["solver"]).jl"`, wait=false))
        git_dirty = git_dirty || git_dirt
        if git_dirt
            println("$progname: The file $sol.jl is not commited to git.")
        end
        if git_dirty && (parsed_args["new"] || !(parsed_args["loadfile"] == ""))
            println("$progname: Make sure to commit $progname and relevant problem before running!")
            exit(3)
        end
    end

    # Check if the problem loaded has defined a dimension:
    if !haskey(parsed_args, "searchspace")
        println("There has not been a dimension defined for the problem you are running: $(parsed_args["solver"])")
        println("Make sure you add a line defining `parsed_args[\"searchspace\"]`.")
        exit(4)
    end

    # Make sure the logfile path exists. We do it this early, so that user can
    # spot their mistake and kill the run before it has gone too far.
    logdir = dirname(parsed_args["logfile"])
    if !isdir(logdir) && logdir != ""
        println("$progname: Log file directory not found. Creating directory.")
        dir = mkpath(logdir)
        println("$progname: $dir")
    end

    if parsed_args["new"]
        # Get an optimisation-problem
        opt = setup_problem(parsed_args)

        # Loop over the cuts
        for cut in 1:parsed_args["cuts"]

            parsed_args["savefile_actual"] = parsed_args["savefile"] * "_" * string(cut)
            parsed_args["savefile_actual"] = joinpath(parsed_args["savepath"], parsed_args["savefile_actual"])

            # Start a run
            opt, res = start_run(opt, parsed_args)

            # Save run
            save_run(opt, res, parsed_args)

            # Print result
            print_res(res)

            # Log run
            if cut == 1
                log_run(res, parsed_args, :new)
            else
                log_run(res, parsed_args, :cont)
            end

            remove_cuts(parsed_args, cut)

        end

        # Print name of save
        print_save(parsed_args)

        # Finished
        exit()

    elseif !(parsed_args["loadfile"] == "")
        # Load saved file
        opt, res, parsed_args_old = load_file(parsed_args)

        # Various things may have changed, so we have to, when we can, setup a
        # new problem.
        if occursin("de_", parsed_args_old["method"])
            # Only support this for de_ methods for now
            pop = copy(opt.optimizer.population.individuals)
            opt = setup_problem(parsed_args, pop)
        else
            # Verify that the same solver is loaded
            if !(parsed_args_old["solver"] == parsed_args["solver"])
                println("$progname: ERROR: The solver has changed for a non-DE method. Will now exit.")
                exit(4)
            end
        end

        # Print the result
        print_res(res)

        # Loop over the cuts
        for cut in 1:parsed_args["cuts"]

            parsed_args["savefile_actual"] = parsed_args["savefile"] * "_" * string(cut)
            parsed_args["savefile_actual"] = joinpath(parsed_args["savepath"], parsed_args["savefile_actual"])

            # Start a run
            opt, res = start_run(opt, parsed_args)

            # Save run
            save_run(opt, res, parsed_args)

            # Print result
            print_res(res)

            # Log run
            if cut == 1
                log_run(res, parsed_args, :load)
            else
                log_run(res, parsed_args, :cont)
            end

            remove_cuts(parsed_args, cut)

        end

        # Print name of save
        print_save(parsed_args)

        # Finished
        exit()
    elseif !(parsed_args["printfile"] == "")
        print_file(parsed_args)
        exit()
    else
        if !parsed_args["quiet"]
            print(
            """
            $progname: You appear to not have activated any functionality.
            """
            )
        end
    end
end

################################################################################
                             #####  #    # #    #
                             #    # #    # ##   #
                             #    # #    # # #  #
                             #####  #    # #  # #
                             #   #  #    # #   ##
                             #    #  ####  #    #
################################################################################

# Parse the arguments
parsed_args = get_arguments()

include(get_solver_filename(parsed_args))

main(parsed_args)
